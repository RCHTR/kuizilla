<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Kuizilla</title>

        <!-- CSS-->
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>asset/css/custom.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.dataTables.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/dataTables.bootstrap.css">
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Bree+Serif|Open+Sans" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .form-group {
                padding: 10px;
            }
            .close {
                margin: 10px;
            }
            .modal-content {
                background: #f9ffa4;
            }
        </style>
    </head>
    <body class="body-luar">
        <?php $this->load->view('private-header'); ?>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align:center">
                        <div class="row">
                            <div class="col-md-4 col-md-offset-4">
                                <img src="<?php echo base_url(); ?>asset/images/IPA.png" class="img-responsive">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <h3>Uji Level IPA</h3>
                            <form onsubmit="event.preventDefault();">
                                <div class="form-group" style="text-align:center">
                                    <div class="col-sm-6 col-md-offset-3">
                                        <input type="text" class="form-control" id="kode_uji" required>
                                        <span class="form-highlight"></span>
                                        <span class="form-bar"></span>
                                        <label class="float-label" for="exampleInputEmail1">Masukkan Kode Uji Level</label>
                                    </div>
                                </div>
                                <br>
                                <center>
                                    <a href="<?php echo base_url(); ?>course/participants?t=18" id="btnMasuk" class="btn btn-default" style="background-color:#38cf94;border-color:#38cf94;border-radius:16px;color:#fff">MASUK</a>
                                    <h4>atau</h4>
                                    <a href="<?php echo base_url(); ?>course/exam?t=18" class="btn btn-primary" style="background-color:#6797ff;border-color: #6797ff;border-radius: 16px;margin-bottom:20px">MULAI TANPA KODE</a><br>
                                </center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#btnMasuk').prop('disabled', false);
            });
        </script>
    </body>
</br>