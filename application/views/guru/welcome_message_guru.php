<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Kuizilla</title>

        <!-- CSS-->
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>asset/css/custom.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.dataTables.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/dataTables.bootstrap.css">
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Bree+Serif|Open+Sans" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .form-group {
                padding: 10px;
            }
        </style>
    </head>
    <body class="body-luar">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <img src="<?php echo base_url(); ?>asset/images/Logo Sisvva.png" style="width:5%" class="img-responsive pull-left">
                        <span class="glyphicon glyphicon-option-vertical pull-right" aria-hidden="true" style="font-size: 22px;color:#6da1ee"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
        <form role="form" onsubmit="event.preventDefault();">
            <div class="setup-content" id="step-2">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h4 style="text-align:center;margin-bottom:50px">Login Guru</h4>
                                <form role="form">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="nip" required>
                                        <span class="form-highlight"></span>
                                        <span class="form-bar"></span>
                                        <label class="float-label" for="exampleInputEmail1">NIP</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="password" required>
                                        <span class="form-highlight"></span>
                                        <span class="form-bar"></span>
                                        <label class="float-label" for="exampleInputEmail1">Password</label>
                                    </div>
                                    <center><button class="btn btn-primary" id="loginBtn_guru" style="margin:15px;font-size:25px;border-radius:25px;background-color: #fb6912;border-color: #fb6912;">LOGIN</button></center>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                </div>
            </div>
            <div class="setup-content" id="step-3">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-body" style="text-align:center">
                                <span style="font-size:20px">Login Berhasil!</span>
                                <div style="width:50%;margin:auto;padding:10px">
                                    <img src="<?php echo base_url(); ?>asset/images/Maskot Login Berhasil.png" class="img-responsive">
                                </div>
                                <span style="font-size:18px">Halo,</span>
                                <h2 style="margin-top: 5px;font-weight: bolder;" id="nama_guru"></h2>
                                <!-- <span id="nip_guru" style="font-size:18px"></span><br> -->
                                <span id="nama_sekolah" style="font-size:18px"></span><br>
                                <span id="propinsi_sekolah" style="font-size:18px"></span>
                                <br>
                                <br>
                                <button type="button" class="btn btn-primary" id="nextButton" style="font-size:25px;border-radius:25px">LANJUT</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                </div>
            </div>
        </form>
        <script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#step-3').hide();
                
                // $('#startBtn').click(function(){

                //     $('#step-2').show();
                //     $('#step-3').hide();
                // });

                $('#loginBtn_guru').click(function(){
                    var nip = $('#nip').val();
                    var password = $('#password').val();
                    console.log('nip', nip)
                    console.log('password', password)
                    $.ajax({
                        method: "POST",
                        url: "<?=base_url('auth/login_guru')?>",
                        data: { username: nip, password: password, type: 'guru' }
                    })
                    .done(function( msg ) {
                        console.log(msg)
                        if (msg.status) {
                            $('#nama_guru').text("Pak "+msg.data[0].nama_guru);
                            $('#nip_guru').text("NIP : "+msg.data[0].nip);
                            $('#nama_sekolah').text("Guru "+msg.data[0].nama_sekolah);
                            $('#propinsi_sekolah').text(msg.data[0].propinsi_sekolah);
                            $('#step-2').hide();
                            $('#step-3').show();
                        } else {
                            alert(msg.message);
                        }
                    });
                });

                $('#nextButton').click(function(){
                    window.location = "<?php echo base_url(); ?>homepage/index_guru";
                });
                
            });

            function clickHandlerLabel(e) {
                var id = this.getAttribute('for');
                var i = wskCheckboxes.length;
                while (i--) {
                if (wskCheckboxes[i].id === id) {
                    if (wskCheckboxes[i].checkbox.className.indexOf('checked') < 0) {
                    wskCheckboxes[i].checkbox.className += ' checked';
                    } else {
                    wskCheckboxes[i].checkbox.className = 'chk-span';
                    }
                    break;
                }
                }
            }
        </script>
    </body>
</br>