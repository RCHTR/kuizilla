<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Kuizilla</title>

        <!-- CSS-->
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>asset/css/custom.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.dataTables.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/dataTables.bootstrap.css">
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Bree+Serif|Open+Sans" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .form-group {
                padding: 10px;
            }
            .close {
                margin: 10px;
            }
            .modal-content {
                background: #f9ffa4;
            }
            .btn-block {
                padding: 5% 0;
            }
        </style>
    </head>
    <body class="body-luar">
        <?php $this->load->view('private-header'); ?>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align:center">
                        <div class="row" style="margin: 5px;background-color: #660066;color:#fff;max-height:600px;overflow:auto">
                            <div class="row" style="margin: 5px;">
                                <div class="col-md-2" style="font-size: 24px;top: 27px;">
                                    <a href="<?=base_url('journey?mp=').$prev?>"><span class="glyphicon glyphicon-chevron-left"></span></a>
                                </div>
                                <div class="col-md-8">
                                    <h3><img src="<?php echo base_url(); ?>asset/images/<?= $mapel->nama ?>.png" style="width:15%">&nbsp;<?=$mapel->nama?></h3>
                                </div>
                                <div class="col-md-2" style="font-size: 24px;top: 27px;">
                                    <a href="<?= base_url('journey?mp=') . $next ?>"><span class="glyphicon glyphicon-chevron-right"></span></a>
                                </div>
                            </div>
                            <?php
                                foreach ($bab as $value) {
                            ?>
                            <div class="row" style="margin: 5px;color:#000;">
                                <div class="panel panel-default" style="background-color: #FFF000;margin-left: 14px;margin-right: 14px;">
                                    <div class="panel-body">
                                        <h3><?=$value->nama?></h3>
                                        <h4><?= $value->bab ?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin: 5px;color:#000;">
                                <div class="col-md-6">
                                    <div class="panel panel-default" style="background-color: #6699ff;border-color:#6699ff">
                                            <h3>Bab I - Level 1</h3>
                                        <?php if($value->level_1==1 ){ ?>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;"></span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                        <?php }elseif($value->level_1 ==2 ){ ?>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;"></span>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;">
                                            </span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                        <?php } elseif($value->level_1 ==3 ){ ?>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;"></span>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;">
                                            </span>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;">
                                            </span>
                                        <?php } elseif($value->level_1 ==0 ) {?>
                                            <span class="glyphicon glyphicon-star-empty""></span></span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                        <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel panel-default" style="background-color: #6699ff;border-color:#6699ff">
                                            <h3>Bab I - Level 2</h3>
                                        <?php if($value->level_2==1 ){ ?>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;"></span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                        <?php }elseif($value->level_2 ==2 ){ ?>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;"></span>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;">
                                            </span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                        <?php } elseif($value->level_2 ==3 ){ ?>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;"></span>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;">
                                            </span>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;">
                                            </span>
                                        <?php } elseif($value->level_2 ==0 ) {?>
                                            <span class="glyphicon glyphicon-star-empty""></span></span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                        <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin: 5px;color:#000;">
                                <div class="col-md-6">
                                    <div class="panel panel-default" style="background-color: #6699ff;border-color:#6699ff">
                                        <div class="panel-body">
                                            <h3>Bab I - Level 3</h3>
                                            <?php if($value->level_3==1 ){ ?>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;"></span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                        <?php }elseif($value->level_3 ==2 ){ ?>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;"></span>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;">
                                            </span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                        <?php } elseif($value->level_3 ==3 ){ ?>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;"></span>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;">
                                            </span>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;">
                                            </span>
                                        <?php } elseif($value->level_3 ==0 ) {?>
                                            <span class="glyphicon glyphicon-star-empty""></span></span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                        <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel panel-default" style="background-color: #6699ff;border-color:#6699ff">                                        <div class="panel-body">
                                            <h3>Bab I - Level 4</h3>
                                            <?php if($value->level_4==1 ){ ?>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;"></span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                        <?php }elseif($value->level_4 ==2 ){ ?>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;"></span>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;">
                                            </span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                        <?php } elseif($value->level_4 ==3 ){ ?>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;"></span>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;">
                                            </span>
                                            <span class="glyphicon glyphicon-star" style="color: #FFF000;">
                                            </span>
                                        <?php } elseif($value->level_4 ==0 ) {?>
                                            <span class="glyphicon glyphicon-star-empty""></span></span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                            <span class="glyphicon glyphicon-star-empty""></span>
                                            </span>
                                        <?php  }  ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $level_4_sblm = $value->level_4; } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {


            });

            function openDialog(course) {
                console.log(course)
            }
        </script>
    </body>
</br>