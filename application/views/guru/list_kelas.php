<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Kuizilla</title>

        <!-- CSS-->
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>asset/css/custom.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.dataTables.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/dataTables.bootstrap.css">
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Bree+Serif|Open+Sans" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .form-group {
                padding: 10px;
            }
            .close {
                margin: 10px;
            }
            .modal-content {
                background: #f9ffa4;
            }
        </style>
    </head>
    <body class="body-luar">
        <?php $this->load->view('private-header'); ?>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align:center">
                        <div class="row">
                            <div class="col-md-4 col-md-offset-2">
                                <img src="<?php echo base_url(); ?>asset/images/avatar.png" class="img-responsive ">
                            </div>
                            <div class="col-md-6" style="text-align:left">
                                <h3><?=$guru->nama_guru?></h3>
                                NIP : <?=$guru->nip?><br>
                                <?=$guru->nama_sekolah?><br>
                                <span class="glyphicon glyphicon-user">&nbsp;5 Pengikut</span>
                            </div>
                        </div>
                        <h2 style="font-weight: bold;" >Kelas</h2>
                        <?php foreach ($kelas_guru as $list) { ?>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 list-kelas-box">
                                <span class="list-kelas-nama"><?php echo $list->nama ?></span>
                                <a href="" style="text-decoration: none;">
                                    <span style="background-color: rgba(232, 65, 24,0.8);padding-left: 20px;padding-right: 20px;" class="list-kelas">Edit</span>
                                </a>
                                <a href="<?=base_url('Homepage/uji_guru/'.$list->id)?>" style="text-decoration: none;">
                                    <span style="background-color: #192a56;padding-left: 10px;padding-right: 10px;" class="list-kelas">Uji Kelas</span>
                                </a>
                                <a href="" style="text-decoration: none;">
                                    <span style="float: right; line-height: 2.7;">[hapus]</span>
                                </a>
                                
                            </div>
                        </div>
                        <?php } ?>
                        <div>
                            <div class="row" style="padding:23px;">
                            <a href="#" type="button" style="font-size:20px;border-radius:15px;background-color:white;width: 350px;" class="btn btn-default">Buat Kelas</a>
                        </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>

        <script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                
                if( $('#pilih-kelas').children("option:selected").val() != "---"){
                    $('#list-rapor-kelas').show();
                }

            });

            function openDialog(course) {
                console.log(course)
            }
        </script>
    </body>
</br>