<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Kuizilla</title>

        <!-- CSS-->
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>asset/css/custom.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.dataTables.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/dataTables.bootstrap.css">
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Bree+Serif|Open+Sans" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .form-group {
                padding: 10px;
            }
            .close {
                margin: 10px;
            }
            .modal-content {
                background: #f9ffa4;
            }
        </style>
    </head>
    <body class="body-luar">
        <?php $this->load->view('private-header'); ?>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align:center">
                        <div class="row">
                            <div class="col-md-4 col-md-offset-2">
                                <img src="<?php echo base_url(); ?>asset/images/avatar.png" class="img-responsive ">
                            </div>
                            <div class="col-md-6" style="text-align:left">
                                <h3><?=$guru->nama_guru?></h3>
                                NIP : <?=$guru->nip?><br>
                                <?=$guru->nama_sekolah?><br>
                                <span class="glyphicon glyphicon-user">&nbsp;5 Pengikut</span>
                            </div>
                        </div>
                        <h2 style="font-weight: bold;" >Rapor</h2>
                        <br>
                        <div>
                            <select class="pilih-rapor" name="" id="pilih-kelas">
                                <option value="---">---</option>
                                <?php foreach ($kelas_guru as $list) { ?>
                                    <option value=""><?php echo $list->nama ?></option>
                                <?php } ?>
                            </select>
                            <br>
                            <h4>Silahkan pilih kelas anda</h4>
                        </div>
                        <br>
                        <div>
                            <select class="pilih-rapor" name="" id="pilih-mapel">
                                <option value="---">---</option>
                                <?php foreach ($mapel as $list) { ?>
                                    <option value=""><?php echo $list->nama ?></option>
                                <?php } ?>
                            </select>
                            <br>
                            <h4>Silahkan pilih mata pelajaran</h4>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="list-rapor-kelas">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align:center">
                        <div class="row">
                            <div class="col-md-4 col-md-offset-2">
                                <img src="<?php echo base_url(); ?>asset/images/avatar.png" class="img-responsive ">
                            </div>
                            <div class="col-md-6" style="text-align:left">
                                <h3><?=$guru->nama_guru?></h3>
                                NIP : <?=$guru->nip?><br>
                                <?=$guru->nama_sekolah?><br>
                                <span class="glyphicon glyphicon-user">&nbsp;5 Pengikut</span>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1" style="overflow:auto;max-height:300px">
                                <h4 style="font-weight: bold; float: left" >Rapor Matematika</h4>
                                <a href="#" style="text-decoration: none">
                                    <h4>[ unduh rapor ]</h4>
                                </a>
                                <table class="table table-bordered table-rapor-guru">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;min-width: 150px;">Nama</th>
                                            <th style="text-align: center;min-width: 50px;">Kelas</th>
                                            <th style="text-align: center;min-width: 70px;">Uji Level</th>
                                            <th style="text-align: center;min-width: 50px;">Bab I</th>
                                            <th style="text-align: center;min-width: 50px;">Bab II</th>
                                            <th style="text-align: center;min-width: 60px;">Bab III</th>
                                            <th style="text-align: center;min-width: 60px;">Bab IV</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Michael Probodanu</td>
                                            <td>9c</td>
                                            <td>60</td>
                                            <td>100</td>
                                            <td>100</td>
                                            <td>100</td>
                                            <td>100</td>
                                        </tr>
                                        <tr>
                                            <td>Michael Probodanu</td>
                                            <td>9c</td>
                                            <td>60</td>
                                            <td>100</td>
                                            <td>100</td>
                                            <td>100</td>
                                            <td>100</td>
                                        </tr>
                                        <tr>
                                            <td>Michael Probodanu</td>
                                            <td>9c</td>
                                            <td>60</td>
                                            <td>100</td>
                                            <td>100</td>
                                            <td>100</td>
                                            <td>100</td>
                                        </tr>
                                        <tr>
                                            <td>Michael Probodanu</td>
                                            <td>9c</td>
                                            <td>60</td>
                                            <td>100</td>
                                            <td>100</td>
                                            <td>100</td>
                                            <td>100</td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <a href="<?=base_url('Homepage/index_guru')?>" style="text-decoration: none">
                                <h4>[Kembali ke halaman utama]</h4>
                            </a>
                            <span id="demografi"></span>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>


        <script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#list-rapor-kelas').show();
                document.getElementById("demografi").innerHTML = $('#pilih-mapel :selected').text();
                if($('#pilih-mapel :selected').text() != '---'){
                    $('#list-rapor-kelas').show();
                }

            });

            function openDialog(course) {
                console.log(course)
            }
        </script>
    </body>
</br>