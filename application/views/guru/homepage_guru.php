<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Kuizilla</title>

        <!-- CSS-->
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>asset/css/custom.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.dataTables.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/dataTables.bootstrap.css">
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Bree+Serif|Open+Sans" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .form-group {
                padding: 10px;
            }
            .close {
                margin: 10px;
            }
            .modal-content {
                background: #f9ffa4;
            }
        </style>
    </head>
    <body class="body-luar">
        <?php $this->load->view('private-header'); ?>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align:center">
                        <div class="row">
                            <div class="col-md-4 col-md-offset-2">
                                <img src="<?php echo base_url(); ?>asset/images/avatar.png" class="img-responsive ">
                            </div>
                            <div class="col-md-6" style="text-align:left">
                                <h3><?=$guru->nama_guru?></h3>
                                NIP : <?=$guru->nip?><br>
                                <?=$guru->nama_sekolah?><br>
                                <span class="glyphicon glyphicon-user">&nbsp;5 Pengikut</span>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-md-1">
                                &nbsp;
                            </div>
                            <div class="col-md-3">
                                <a href="<?=base_url('Homepage/list_kelas_guru')?>" style="text-decoration: none;">
                                    <div class="menu-guru-kelas">
                                        <span class="menu-guru-label">Kelas</span>
                                    </div>
                                </a>
                               <!--  <img src="<?php echo base_url(); ?>asset/images/<?=$image[15]?>" class="img-responsive"><br>Kelas -->
                            </div>
                            <div class="col-md-2">
                                &nbsp;
                            </div>
                            <div class="col-md-3">
                                <a href="<?=base_url('rapor/rapor_guru')?>" style="text-decoration: none;">
                                    <div class="menu-guru-rapor">
                                        <span class="menu-guru-label">Rapor</span>
                                    </div>
                                </a>
                               <!--  <img src="<?php echo base_url(); ?>asset/images/<?=$image[15]?>" class="img-responsive"><br>Kelas -->
                            </div>
                            <div class="col-md-1">
                                &nbsp;
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>

        <!-- SCIENCE MODAL DIALOG -->

        <script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {


            });

            function openDialog(course) {
                console.log(course)
            }
        </script>
    </body>
</br>