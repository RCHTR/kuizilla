<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Kuizilla</title>

        <!-- CSS-->
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>asset/css/custom.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.dataTables.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/dataTables.bootstrap.css">
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Bree+Serif|Open+Sans" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .form-group {
                padding: 10px;
            }
            .close {
                margin: 10px;
            }
            .modal-content {
                background: #f9ffa4;
            }
        </style>
    </head>
    <body class="body-luar">
        <?php $this->load->view('private-header'); ?>
        <div class="row" id="side-jenis-uji">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align:center">
                        <div class="row">
                            <div class="col-md-4 col-md-offset-2">
                                <img src="<?php echo base_url(); ?>asset/images/avatar.png" class="img-responsive ">
                            </div>
                            <div class="col-md-6" style="text-align:left">
                                <h3><?=$guru->nama_guru?></h3>
                                NIP : <?=$guru->nip?><br>
                                <?=$guru->nama_sekolah?><br>
                                <span class="glyphicon glyphicon-user">&nbsp;5 Pengikut</span>
                            </div>
                        </div>
                        <h2 style="font-weight: bold;" >Uji Kelas </h2>
                        <br>
                        <div class="row">
                            <div class="col-md-4 col-md-offset-4 jenis-ujian" id="click-uji-level">
                                <span>Uji Level</span>
                            </div>
                        </div>
                        <div class="row">
                            <a href="">
                            <div class="col-md-4 col-md-offset-4 jenis-ujian" id="click-ulangan">
                                <span>Ulangan</span>
                            </div>
                            </a>
                        </div>
                        <div class="row">
                            <a href="">
                            <div class="col-md-4 col-md-offset-4 jenis-ujian" id="click-tryout">
                                <span>Tryout</span>
                            </div>
                            </a>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="side-uji-level">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align:center">
                        <div class="row">
                            <div class="col-md-4 col-md-offset-2">
                                <img src="<?php echo base_url(); ?>asset/images/avatar.png" class="img-responsive ">
                            </div>
                            <div class="col-md-6" style="text-align:left">
                                <h3><?=$guru->nama_guru?></h3>
                                NIP : <?=$guru->nip?><br>
                                <?=$guru->nama_sekolah?><br>
                                <span class="glyphicon glyphicon-user">&nbsp;5 Pengikut</span>
                            </div>
                        </div>
                        <h2 style="font-weight: bold;" >Uji Level <?php echo $kelas->nama ?></h2>
                        <h4 style="" >Pilih Pelajaran</h4>
                        <div class="row list-mapel-uji">
                        <!-- <?php foreach ($mapel as $list) { ?>
                            <div class="col-md-5 col-md-offset-5 nama-mapel" >
                                <span><?php echo $list->nama ?></span>
                            </div>
                        <?php } ?> -->
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#mathDialog">
                                <div class="col-md-5 col-md-offset-5 nama-mapel" >
                                    <span>Matematika</span>
                                </div>
                            </a>
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#ipaDialog">
                                <div class="col-md-5 col-md-offset-6 nama-mapel" >
                                    <span>IPA</span>
                                </div>
                            </a>
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#indoDialog">
                                <div class="col-md-5 col-md-offset-5 nama-mapel" >
                                    <span>Bahasa Indonesia</span>
                                </div>
                            </a>
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#inggrisDialog">
                                <div class="col-md-5 col-md-offset-5 nama-mapel" >
                                    <span>Bahasa Inggris</span>
                                </div>
                            </a>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>

        <!-- MATH MODAL DIALOG -->
        <div id="mathDialog" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document" style="width: 400px;">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="row" style="text-align:center" style="margin-top:10px">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 style="font-weight: bold;">Uji Level <?php echo $kelas->nama ?></h2>
                        </div>
                    </div>
                    <div class="row" style="margin-top:10px">
                        <div class="col-md-4 col-md-offset-4">
                            <img src="<?php echo base_url(); ?>asset/images/Matematika.png" class="img-responsive">
                        </div>
                    </div>
                    <div class="row" style="text-align:center">
                        <div class="col-md-8 col-md-offset-2">
                            <h3>Matematika</h3>
                            <p>10 Soal, durasi: 30 Menit</p>
                        </div>
                    </div>
                    <div class="row" style="text-align:center">
                        <select class="pilih-rapor" name="" id="pilih-kelas">
                            <option value="---">---</option>
                            <?php foreach ($bab as $list) { ?>
                                <option value="<?php echo $list->id ?>"><?php echo $list->nama ?></option>
                            <?php } ?>
                        </select>
                        <br>
                        <span>Silahkan pilih Bab</span>
                    </div>
                    <div class="row" style="text-align:center;margin:10px">
                        <div class="col-md-4 col-md-offset-4">
                            <a href="<?=base_url('Homepage/uji_level/'.$kelas->id.'/15')?>" type="button" style="font-size:25px;border-radius:25px" class="btn btn-primary">MULAI</a>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- IPA MODAL DIALOG -->
        <div id="ipaDialog" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document" style="width: 400px;">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="row" style="text-align:center" style="margin-top:10px">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 style="font-weight: bold;">Uji Level <?php echo $kelas->nama ?></h2>
                        </div>
                    </div>
                    <div class="row" style="margin-top:10px">
                        <div class="col-md-4 col-md-offset-4">
                            <img src="<?php echo base_url(); ?>asset/images/IPA.png" class="img-responsive">
                        </div>
                    </div>
                    <div class="row" style="text-align:center">
                        <div class="col-md-8 col-md-offset-2">
                            <h3>Ilmu Pengetahuan Alam</h3>
                            <p>10 Soal, durasi: 30 Menit</p>
                        </div>
                    </div>
                    <div class="row" style="text-align:center;margin:10px">
                        <div class="col-md-4 col-md-offset-4">
                            <a href="<?=base_url('Homepage/uji_level/'.$kelas->id.'/18')?>" type="button" style="font-size:25px;border-radius:25px" class="btn btn-primary">MULAI</a>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- Indo MODAL DIALOG -->
        <div id="indoDialog" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document" style="width: 400px;">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="row" style="text-align:center" style="margin-top:10px">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 style="font-weight: bold;">Uji Level <?php echo $kelas->nama ?></h2>
                        </div>
                    </div>
                    <div class="row" style="margin-top:10px">
                        <div class="col-md-4 col-md-offset-4">
                            <img src="<?php echo base_url(); ?>asset/images/Bahasa Indonesia.png" class="img-responsive">
                        </div>
                    </div>
                    <div class="row" style="text-align:center">
                        <div class="col-md-8 col-md-offset-2">
                            <h3>Bahasa Indonesia</h3>
                            <p>10 Soal, durasi: 30 Menit</p>
                        </div>
                    </div>
                    <div class="row" style="text-align:center;margin:10px">
                        <div class="col-md-4 col-md-offset-4">
                            <a href="<?=base_url('Homepage/uji_level/'.$kelas->id.'/16')?>" type="button" style="font-size:25px;border-radius:25px" class="btn btn-primary">MULAI</a>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- Inggris MODAL DIALOG -->
        <div id="inggrisDialog" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document" style="width: 400px;">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="row" style="text-align:center" style="margin-top:10px">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 style="font-weight: bold;">Uji Level <?php echo $kelas->nama ?></h2>
                        </div>
                    </div>
                    <div class="row" style="margin-top:10px">
                        <div class="col-md-4 col-md-offset-4">
                            <img src="<?php echo base_url(); ?>asset/images/Bahasa Inggris.png" class="img-responsive">
                        </div>
                    </div>
                    <div class="row" style="text-align:center">
                        <div class="col-md-8 col-md-offset-2">
                            <h3>Bahasa Inggris</h3>
                            <p>10 Soal, durasi: 30 Menit</p>
                        </div>
                    </div>
                    <div class="row" style="text-align:center;margin:10px">
                        <div class="col-md-4 col-md-offset-4">
                            <a href="<?=base_url('Homepage/uji_level/'.$kelas->id.'/20')?>" type="button" style="font-size:25px;border-radius:25px" class="btn btn-primary">MULAI</a>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#side-uji-level').show();
                $("#click-uji-level").click(function(){
                    $('#side-jenis-uji').show();
                    $('#side-uji-level').show();
                });

            });
            function openDialog(course) {
                console.log(course)
            }
        </script>
    </body>
</br>