<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Kuizilla</title>

        <!-- CSS-->
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>asset/css/custom.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.dataTables.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/dataTables.bootstrap.css">
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Bree+Serif|Open+Sans" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .form-group {
                padding: 10px;
            }
            .close {
                margin: 10px;
            }
            .modal-content {
                background: #f9ffa4;
            }
        </style>
    </head>
    <body class="body-luar">
        <?php $this->load->view('private-header'); ?>
        <div class="row" id="side-uji-list-peserta">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align:center">
                        <h3 style="font-weight: bold; margin-top: 10px" >Uji Level <?php echo $kelas->nama ?> </h3>
                        <h4 style="margin-top: 10px;margin-bottom: 5px;">KODE:</h4>
                        <h1 class="kode-guru-uji-level"><?php echo $Kode_rand ?></h1>
                        <span>Peserta</span>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1" style="overflow:auto;max-height:300px">
                                <table class="table table-bordered table-peserta-level-kelas">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Lengkap</th>
                                            <th>Kelas</th>
                                            <th>NIS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Michael Probodanu</td>
                                            <td>9c</td>
                                            <td>123124124</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Michael Probodanu</td>
                                            <td>9c</td>
                                            <td>123124124</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Michael Probodanu</td>
                                            <td>9c</td>
                                            <td>123124124</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Michael Probodanu</td>
                                            <td>9c</td>
                                            <td>123124124</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Michael Probodanu</td>
                                            <td>9c</td>
                                            <td>123124124</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Michael Probodanu</td>
                                            <td>9c</td>
                                            <td>123124124</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Michael Probodanu</td>
                                            <td>9c</td>
                                            <td>123124124</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Michael Probodanu</td>
                                            <td>9c</td>
                                            <td>123124124</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Michael Probodanu</td>
                                            <td>9c</td>
                                            <td>123124124</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <br>
                        <div class="row" style="text-align:center;margin-right: 10px">
                            <div class="col-md-4 col-md-offset-4">
                                <a href="#" type="button" class="btn btn-mulai-uji-level" id="click-mulai-uji-level">Mulai Uji Level</a>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" id="side-uji-level">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default" style="background-color: rgba(238, 90, 36,0.9);border: none;">
                    <div class="panel-body" style="text-align:center">
                        <h3 style="font-weight: 900; margin-top: 10px" >Uji Level <?php echo $kelas->nama ?> </h3>
                        <span style="margin:10px;font-size: 20px;font-weight: 900;"><?php echo $mapel_uji->nama ?></span><br>
                        <span style="margin-top: 15px;margin-bottom: 5px;">KODE:</span>
                        <h2 style="margin-top: 5px;font-weight: 900;"><?php echo $Kode_rand ?></h2>
                        <br>
                        <span class="countdown-uji-level" id="countdown-time"></span>
                        <br>
                        <br>
                        <div class="row" style="text-align:center;margin-right: 10px">
                            <div class="col-md-4 col-md-offset-4">
                                <span type="button"  style="margin-left: 15px;" class="btn btn-stop-uji-level" id="click-force-stop-uji-level">Stop Uji Level</span>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>

        <!-- Force Waktu Selesai DIALOG -->
        <div id="ujilevelForceDone" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document" style="width: 400px; margin-top: 100px;">
                <div class="modal-content" style="background: white; border-radius: 25px;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="row" style="text-align:center" style="margin-top:10px">
                        <div class="col-md-8 col-md-offset-2">
                            <h3 style="margin-top: 60px;">Waktu Uji Level belum berakhir. Anda yakin ingin mengakhiri Uji Level?</h3>
                        </div>
                    </div>
                    <div class="row" style="text-align:center;margin-left:30px; margin-top: 20px;">
                        <div class="col-md-4 col-md-offset-1">
                            <a href="" type="button" class="btn btn-selesai-uji-level" data-dismiss="modal">
                                <span style="margin-left: 20px;margin-right: 20px;">Kembali</span>
                            </a>
                        </div>
                        <div class="col-md-4 col-md-offset-1">
                            <button type="button" class="btn btn-force-selesai-uji-level" id="yes-force-stop-uji-level"><span style="margin-left: 20px;margin-right: 20px;">Ya</span></button>
                        </div>
                    </div>
                    <br>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- Waktu Selesai DIALOG -->
        <div id="ujilevelDone" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document" style="width: 500px;">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="row" style="margin-top:10px">
                        <div class="col-md-4 col-md-offset-4">
                            <img src="<?php echo base_url(); ?>asset/images/Matematika.png" class="img-responsive">
                        </div>
                    </div>
                    <div class="row" style="text-align:center" style="margin-top:10px">
                        <div class="col-md-8 col-md-offset-2">
                            <h3>Uji Level Telah Berakhir</h3>
                        </div>
                    </div>
                    <div class="row" style="text-align:center;margin:10px">
                        <div class="col-md-4 col-md-offset-4">
                            <a href="<?=base_url('Homepage/hasil_uji/'.$kelas->id.'/'.$mapel_uji->id)?>" type="button" class="btn btn-selesai-uji-level"><span style="margin-left: 20px;margin-right: 20px;">Lanjut</span></a>
                        </div>
                    </div>
                    <div class="row" style="text-align:center;margin:10px">
                        <a href="<?=base_url('Homepage/index_guru')?>">
                            <span>[Kembali ke Halaman Utama]</span>
                        </a>
                    </div>
                    <br>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#side-uji-list-peserta').show();
                $('#side-uji-level').hide();
                $("#click-mulai-uji-level").click(function(){
                    $('#side-uji-list-peserta').hide();
                    $('#side-uji-level').show();
                });

            });
            $(document).ready(function () {
                $("#click-force-stop-uji-level").click(function(){
                    $('#ujilevelForceDone').modal('show');
                });

            });
            function openDialog(course) {
                console.log(course)
            }
        </script>
        <script>
            $(document).ready(function () {
                $("#click-mulai-uji-level").click(function(){
                var countDownDate = new Date().getTime() + 3*60*1000 + 1*1000;
                // Update the count down every 1 second
                var x = setInterval(function() {
                    // Get todays date and time
                    var now = new Date().getTime();
                    // Find the distance between now and the count down date
                    var distance = countDownDate - now;
                    // Time calculations for days, hours, minutes and seconds
                    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                    // Output the result in an element with id="demo"
                    document.getElementById("countdown-time").innerHTML = days + ":" + hours + ":"
                    + minutes + ":" + seconds ;
                    // If the count down is over, write some text
                    if (distance < 0) {
                        clearInterval(x);
                        document.getElementById("countdown-time").innerHTML = "EXPIRED";
                        $('#ujilevelDone').modal('show');
                    }
                }, 1000);
                $("#yes-force-stop-uji-level").click(function(){
                    $('#ujilevelForceDone').modal('hide');
                    $('#ujilevelDone').modal('show');
                    clearInterval(x);
                });
                });

            });
            // Set the date we're counting down to
            
            </script>
    </body>
</br>