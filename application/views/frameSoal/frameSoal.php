<script type="text/javascript">
    $(document).ready(function () {
        $('#dataSoal').DataTable(
                {
                    searching: false,
                    ordering: false,
                    lengthMenu: [4, 8, 16],
                    lengthChange: false,
                    info: false,
                    language: {lengthMenu: "show _MENU_ records"}
                });
    });
</script>
<div class="panel-body panel-top">

    <div class="sortir-bs">
        <!--<span><p>Sortir:</p></span>-->
        <table>
            <tr class="top-sortir">
<!--                <td><a href="<?php echo base_url(); ?>index.php/Frame_Soal/getOrder/kelas">Kelas &nbsp<span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a></td>
                <td><a href="<?php echo base_url(); ?>index.php/Frame_Soal/getOrder/subjek">Subyek &nbsp<span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a></td>
                <td><a href="<?php echo base_url(); ?>index.php/Frame_Soal/getOrder/nama_type_soal">Tipe &nbsp<span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a></td>-->
                <td width="20%" rowspan="2">
                    <a href="<?php echo base_url().'index.php/Frame_Soal';?>">
                        <img src="<?php echo base_url();?>asset/Logo KuiziCard.png" alt="Kuizilla">
      </a>
                </td>
                <td width="40%"></td>
                <td>
                    <div class="form-group">
                        <form class="input-group" action="<?php echo base_url(); ?>index.php/Frame_Soal/getAllBy" method="post">
                            <input class="form-control" id="tcari" type="text" name="search" placeholder="" required/>
                            <span class="input-group-btn">
                                <button class="btn btn-search-banksoal" type="submit">
                                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                </button>
                            </span>
                        </form>
                    </div>
                    <div><br></div>
                </td>                
            </tr>
            <tr>
                <td colspan="2" style="text-align: right">
                    <div class="mapel-bs">
                        <a href="http://administrator.kuizilla.com/Soal" target="_blank">Kuizi CardKu</a>
                        <?php
                        foreach ($dataMapel as $dt) {
                            echo '<a href="' . base_url() . 'index.php/Frame_Soal/getBy/' . $dt->id . '">' . $dt->nama . '</a>';
                        }
                        ?>

                    </div>
                </td>
            </tr>
        </table>
    </div>

</div>
<?php if (sizeof($dataSoal) < 1) { ?>
    <div class="panel-body bottom-kisi-kisi">
        <img src="<?php echo base_url(); ?>asset/images/Riwayat Pencarian.png">
        <p>Tidak Menemukan Data</p>
    </div>
<?php } else { ?>
    <div class="panel-body panel-bottom">
        <table id="dataSoal">
            <thead>
                <tr>			
                    <th>&nbsp;</th>

                </tr>
            </thead>
            <tbody>
                <tr><td>
                        <ul class="box-bs clearfix">
                            <?php
                            $i = 0;
                            foreach ($dataSoal as $dt) {
                                $sekor=$dt->skor_tertinggi; 
                                $bintang="&#9734; &#9734; &#9734; &#9734; &#9734;";
                                if($sekor>95){
                                    $bintang="&#9733; &#9733; &#9733; &#9733; &#9733;";
                                }else if($sekor>79){
                                    $bintang="&#9733; &#9733; &#9733; &#9733; &#9734;";
                                }else if($sekor>59){
                                    $bintang="&#9733; &#9733; &#9733; &#9734; &#9734;";
                                }else if($sekor>39){
                                    $bintang="&#9733; &#9733; &#9734; &#9734; &#9734;";
                                }else if($sekor>0){
                                    $bintang="&#9733; &#9734; &#9734; &#9734; &#9734;";
                                }
                                $i++;
                                ?>
                                <li>
                                    <a href="<?php echo base_url() . 'index.php/soal/getBy/' . $dt->id ?>"><img width="70" height="40" src="<?php echo $this->config->item('base_url_backend') . 'asset/uploads/' . $dt->logo; ?>" alt=""></a>
                                    <a class="box-bs-name" href="<?php echo base_url() . 'index.php/soal/getBy/' . $dt->id ?>"><?php echo $dt->subjek.'  ' ;?></a>
                                    <br><span class="box-bs-rating" href="#"><?php echo $bintang;?></span>
                                </li>
                                <?php
                                if ($i % 4 == 0) {
                                    echo '</ul></td></tr><tr><td>
                  <ul class="box-bs clearfix">';
                                }
                            }
                            ?>
                        </ul>
                    </td></tr>
            </tbody>
        </table>

    </div>
<?php } ?>