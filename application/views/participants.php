<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Kuizilla</title>

        <!-- CSS-->
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>asset/css/custom.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.dataTables.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/dataTables.bootstrap.css">
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Bree+Serif|Open+Sans" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .form-group {
                padding: 10px;
            }
            .close {
                margin: 10px;
            }
            .modal-content {
                background: #cb1cff;
                color: #fff;
            }
            .table-wrapper-scroll-y {
                display: block;
                max-height: 200px;
                overflow-y: auto;
                -ms-overflow-style: -ms-autohiding-scrollbar;
            }
            .list-group-flush:first-child .list-group-item:first-child {
                border-top: 0;
            }
            .list-group-flush .list-group-item {
                border-right: 0;
                border-left: 0;
                border-radius: 0;
            }
            .list-group-item {
                border: 1.5px solid #bbbbbb;
            }
        </style>
    </head>
    <body class="body-luar">
        <?php $this->load->view('private-header'); ?>
        <br class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row" style="text-align:center">
                            <div class="col-sm-4">
                                <img style="width:40%" src="<?=$icon?>" class="img-responsive pull-right">
                            </div>
                            <div class="col-sm-8" style="padding-top:10px;padding-right:85px">
                                <b>Uji Level <?=$mapel?></b><br>
                                <b><?=$siswa->nama_kelas?> <?=$siswa->nama_sekolah?></b>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div style="width:80%;margin:auto">
                                <b>PESERTA (<?=count($participant)?>/<?=count($participant)?>)</b>
                            </div>
                            <div class="table-wrapper-scroll-y" style="width:80%;margin:auto">
                                <ul class="list-group list-group-flush">
                                    <?php
                                        foreach ($participant as $key => $value) {
                                            $bg = ($value->id == $siswa->id) ? 'style="background:#38cf94;color:#fff"' : '' ;
                                    ?>
                                    <li class="list-group-item" <?=$bg?>><?=$value->nama_siswa?><br><?=$value->nis?><div class="pull-right"></div> </li>
                                    <?php
                                        }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- MODAL DIALOG -->
        <div id="dialog" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="row" style="text-align:center">
                        <div class="col-md-8 col-md-offset-2">
                            <h3>Uji Level <?=$mapel?></h3>
                            <h4>Akan Segera Dimulai!</h4>
                        </div>
                    </div>
                    <div class="row" style="text-align:center;margin:10px">
                        <p id="demo"></p><br>
                        <h3>Semoga Sukses Ya!</h3>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                setTimeout(
                    function(){
                        $('#dialog').modal();
                        countDown(3);
                    }, 3000
                );
            });

            function countDown (timeleft) {
                var downloadTimer = setInterval(function(){
                document.getElementById("demo").innerHTML = --timeleft+1;
                if(timeleft <= 0)
                    clearInterval(downloadTimer);
                if(timeleft == 0)
                    window.location = "<?php echo base_url(); ?>course/exam?t=<?=$soal?>";
                },1000);
            }
        </script>
    </body>
</br>