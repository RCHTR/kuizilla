<?php
    $is_exam = isset($is_exam) ? $is_exam : false;
    $logo_width = ($is_exam) ? 3 : 5; 
    $class_grid = ($is_exam) ? 'col-md-6 col-md-offset-3' : 'col-md-4 col-md-offset-4 col-xs-4 col-xs-offset-4';
?>      
        <div class="row">
            <div class="<?=$class_grid?>">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <a href="<?=base_url()?>">
                            <img src="<?php echo base_url(); ?>asset/images/Logo Sisvva.png" style="width:<?=$logo_width?>%" class="img-responsive pull-left">
                        </a>
                        <div class="pull-right">
                            <a href="javasctipr:void(0);"><span class="glyphicon glyphicon-envelope" style="font-size: 22px;color:#000000;margin-right:15px" aria-hidden="true"></span></a>
                            <span class="glyphicon glyphicon-option-vertical pull-right dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-hidden="true" style="font-size: 22px;color:#6da1ee"></span>
                            <ul class="dropdown-menu">
                                <li><a href="#">Profil</a></li>
                                <li><a href="#">Ubah Password</a></li>
                                <li><a href="<?php echo base_url(); ?>rapor">Rapor</a></li>
                                <li><a href="<?php echo base_url(); ?>homepage/targetHarian">Target Harian</a></li>
                                <li><a href="<?php echo base_url(); ?>journey">Journey</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?= base_url('auth/logout') ?>">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>