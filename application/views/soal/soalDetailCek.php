<script>
    $(document).ready(function () {
        timePicker(10);
    });

    function lanjut() {
        var id = document.getElementById("id").value;
        var nosoal = document.getElementById("nosoal").value;
        var jawabanA = document.getElementById("jawabanA").checked;
        var jawabanB = document.getElementById("jawabanB").checked;
        var jawabanC = document.getElementById("jawabanC").checked;
        var jawabanD = document.getElementById("jawabanD").checked;
        var jawabanE = document.getElementById("jawabanE").checked;
        if ((jawabanA == "" || jawabanA == null) && (jawabanB == "" || jawabanB == null) && (jawabanC == "" || jawabanC == null) && (jawabanD == "" || jawabanD == null) && (jawabanE == "" || jawabanE == null)) {
            return alert("harap pilih jawaban");
        } else {
            var jawaban;
            if (jawabanA) {
                jawaban = "A";
            } else if (jawabanB) {
                jawaban = "B";
            } else if (jawabanC) {
                jawaban = "C";
            } else if (jawabanD) {
                jawaban = "D";
            } else if (jawabanE) {
                jawaban = "E";
            }
            window.location = "<?php echo base_url() . 'index.php/soal/getBySoalNourut/'; ?>" + id + "/" + nosoal + "/" + jawaban;
        }

    }


    function cekJawaban() {
        var id = document.getElementById("id").value;
        var nosoal = <?php echo $nosoal; ?>;
        var jawabanA = document.getElementById("jawabanA").checked;
        var jawabanB = document.getElementById("jawabanB").checked;
        var jawabanC = document.getElementById("jawabanC").checked;
        var jawabanD = document.getElementById("jawabanD").checked;
        var jawabanE = document.getElementById("jawabanE").checked;
        if ((jawabanA == "" || jawabanA == null) && (jawabanB == "" || jawabanB == null) && (jawabanC == "" || jawabanC == null) && (jawabanD == "" || jawabanD == null) && (jawabanE == "" || jawabanE == null)) {
            return alert("harap pilih jawaban");
        } else {
            var jawaban;
            if (jawabanA) {
                jawaban = "A";
            } else if (jawabanB) {
                jawaban = "B";
            } else if (jawabanC) {
                jawaban = "C";
            } else if (jawabanD) {
                jawaban = "D";
            } else if (jawabanE) {
                jawaban = "E";
            }
            window.open("<?php echo base_url() . 'index.php/soal/getJawaban/'; ?>" + id + "/" + nosoal + "/" + jawaban, 'newwindow', 'width=150,height=50,align=center');
        }
    }
</script>

<script>
    // timer code starts here --- 
    //var init2 = 50;
    var s;

    function timePicker(vr) {

        if (vr > 0)
        {
            if (vr > 1) {
                $('#timer').html(' ' + vr + ' seconds');


            } else {

                $('#timer').html('Data will be updated in next 1 secound');
            }
            vr--;
            s = setTimeout('timePicker(' + vr + ')', 1000);
        } else {
            clearInterval(s);

            $.post('data.php', {txt_area: $('#dstr').val()}, function (r) {
                $('#upd_div').html("Last Updated: " + r);
                $('#timer').html('ata will be updated in next 10 secounds');
                s = setTimeout('timePicker(' + 10 + ')', 5000);
                return false;

            });
        }
    }
</script>

<style>
    /* The container */
    .container {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 17px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default radio button */
    .container input {
        position: absolute;
        opacity: 0;
    }

    /* Create a custom radio button */
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 25px;
        width: 25px;
        background-color: #eee;
        border-radius: 50%;
    }

    /* On mouse-over, add a grey background color */
    .container:hover input ~ .checkmark {
        background-color: #ccc;
    }

    /* When the radio button is checked, add a blue background */
    .container input:checked ~ .checkmark {
        background-color: #ec971f;
    }

    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the indicator (dot/circle) when checked */
    .container input:checked ~ .checkmark:after {
        display: block;
    }

    /* Style the indicator (dot/circle) */
    .container .checkmark:after {
        top: 9px;
        left: 9px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background: white;
    }
</style>
<div class="panel-body panel-atas">
    <div class="judulsoal-bs">
        <table>
            <tr>
                <td><img style="margin-left: 40px; height: 40px;width: auto;" src="<?php echo $this->config->item('base_url_backend') . 'asset/uploads/' . $dataSoal->logo; ?>">
                    <p style="margin-top: 10px">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php
                        echo $nosoal;
                        $persen = ($nosoal) / $jumlahSoal * 100;
                        ?>/<?php echo $jumlahSoal; ?></p></td>
                <td><p style="text-align: center;"><?php echo $dataSoal->nama_type_soal . ' ' . $dataSoal->subjek . ' ' . $dataSoal->nama . ' ' . $dataSoal->jenjang . ' Kelas ' . $dataSoal->kelas; ?> <br> <?php echo $dataSoal->nama_meta_sumber; ?></p></td>
                <td><a href="">
                                <img  style="width: 29px; height: 33px; float: right; margin-right: 40px" src="<?php echo base_url();?>asset/images/Button Laporkan.png"></a></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel-body panel-bottom">
    <div class="panel panel-default soal-bs">
        <div class="panel-body">
            <p style=""><?php echo $dataSoalDetail->pertanyaan ?></p>
        </div>
        <div class="soalpilihan-bs">

            <form method="">
                <table><tr>
                        <td>&nbsp;</td>
                        <td>
                            <label class="container"><?php echo $dataSoalDetail->pilihan_a ?>
                                <input type="radio" name="jawaban">
                                <span class="checkmark"></span>
                            </label>
                            <label class="container"><?php echo $dataSoalDetail->pilihan_b ?>
                                <input type="radio" name="jawaban">
                                <span class="checkmark"></span>
                            </label>
                            <?php if($dataSoalDetail->type==2||$dataSoalDetail->type==3){?>
                            <label class="container"><?php echo $dataSoalDetail->pilihan_c ?>
                                <input type="radio" name="jawaban">
                                <span class="checkmark"></span>
                            </label>
                            <label class="container"><?php echo $dataSoalDetail->pilihan_d ?>
                                <input type="radio" name="jawaban">
                                <span class="checkmark"></span>
                            </label>
                            <?php } 
                            if($dataSoalDetail->type==3){?>
                            <label class="container"><?php echo $dataSoalDetail->pilihan_e ?>
                                <input type="radio" name="jawaban">
                                <span class="checkmark"></span>
                            </label>
                            <?php }?>
                        </td></tr></table>
            </form>
        </div>
        <div class="button-bs">
            <form method="">
                <table>
                    <tr>
                        <td id="kisikisi"><a href=""><img src="<?php echo base_url(); ?>asset/images/Cari Kisi-Kisi small.png"></a></td>
                        <td><input type="hidden" id="id" value="<?php echo $id ?>" name="id"/>
                            <input type="hidden" id="nosoal" value="<?php echo $nosoal + 1 ?>" name="nosoal"/></td>
                        <td><input type="button" value="LANJUT" onclick="lanjut()">
                            <input type="button" value="CEK" onclick="cekJawaban()"></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
    <div class="progress">
        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $persen ?>%">
            <span class="sr-only">20% Complete</span>
        </div>
    </div>
    <a href="<?php echo base_url() ?>index.php/Frame_Soal"><p style="font-size: 12px; text-align: center; color: #808080;">Kembali Ke Halama Utama <img src="<?php echo base_url(); ?>asset/images/Button Kembali ke laman utama.png"></p></a>
</div>
