<style>
    /* The container */
    .container {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 2px;
        cursor: pointer;
        font-size: 17px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default radio button */
    .container input {
        position: absolute;
        opacity: 0;
    }

    /* Create a custom radio button */
    .checkmark {
        position: absolute;
        top: 1px;
        left: 0;
        height: 25px;
        width: 25px;
        background-color: #eee;
        border-radius: 50%;
    }

    /* On mouse-over, add a grey background color */
    .container:hover input ~ .checkmark {
        background-color: #ccc;
    }

    /* When the radio button is checked, add a blue background */
    .container input:checked ~ .checkmark {
        background-color: #ec971f;
    }

    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the indicator (dot/circle) when checked */
    .container input:checked ~ .checkmark:after {
        display: block;
    }

    /* Style the indicator (dot/circle) */
    .container .checkmark:after {
        top: 4px;
        left: 9px;
        width: 8px;
        height: 4px;
        border-radius: 50%;
    }
</style>
<div class="panel-body panel-atas">
    <div class="judulsoal-bs">
        <table>
            <tr>
                <td><img style="margin-left: 40px; height: 40px;width: auto;" src="<?php echo $this->config->item('base_url_backend') . 'asset/uploads/' . $dataSoal->logo; ?>">
                    <p style="margin-top: 10px">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <?php
                        echo $nosoal;
                        $persen = ($nosoal-1) / $jumlahSoal * 100;
                        ?>/<?php echo $jumlahSoal; ?></p></td>
                <td><p style="text-align: center;"><?php echo $dataSoal->nama_type_soal . ' ' . $dataSoal->subjek . ' ' . $dataSoal->nama . ' ' . $dataSoal->jenjang . ' Kelas ' . $dataSoal->kelas; ?> <br> <?php echo $dataSoal->nama_meta_sumber; ?></p></td>
                <td><a href="">
                                <img  style="width: 29px; height: 33px; float: right; margin-right: 40px" src="<?php echo base_url();?>asset/images/Button Laporkan.png"></a></td>
            </tr>
            
        </table>
    </div>
</div>
<div class="panel-body panel-bottom">
    <div class="panel panel-default soal-bs">       
        
        <form method="post" action="<?php echo base_url(); ?>index.php/soal/soalCek/">  
            <div style="height: 350px;overflow: auto;">
            <div class="panel-body">
            <?php 
            if(!empty($dataSoalDetail->picture)){
                echo '<img style="margin-left: 80px; text-align: center;height: 150px;width: auto;" src="'.$this->config->item('base_url_backend') . 'asset/soal/' . $dataSoalDetail->picture.'"';
            }?>
            <p style=""><?php echo $dataSoalDetail->pertanyaan ?></p>
        </div>
            <div class="soalpilihan-bs">

                <table><tr>
                        <td>&nbsp;</td>
                        <td>
                            <label class="container"><?php echo $dataSoalDetail->pilihan_a ?>
                                <input type="radio" name="jawaban" value="A">
                                <span class="checkmark"></span>
                            </label>
                            <label class="container"><?php echo $dataSoalDetail->pilihan_b ?>
                                <input type="radio" name="jawaban" value="B">
                                <span class="checkmark"></span>
                            </label>
                            <?php if ($dataSoalDetail->type == 2 || $dataSoalDetail->type == 3) { ?>
                                <label class="container"><?php echo $dataSoalDetail->pilihan_c ?>
                                    <input type="radio" name="jawaban" value="C">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container"><?php echo $dataSoalDetail->pilihan_d ?>
                                    <input type="radio" name="jawaban" value="D">
                                    <span class="checkmark"></span>
                                </label>
                            <?php }
                            if ($dataSoalDetail->type == 3) {
                                ?>
                                <label class="container"><?php echo $dataSoalDetail->pilihan_e ?>
                                    <input type="radio" name="jawaban" value="E">
                                    <span class="checkmark"></span>
                                </label>
<?php } ?>
                            <input type="hidden" id="id" value="<?php echo $id ?>" name="id"/>
                            <input type="hidden" id="nosoal" value="<?php echo $nosoal?>" name="nosoal"/>
                        </td></tr></table>

            </div>
            </div>
            <div class="clearfix"><br>
            </div>
            <div class="button-bs">
                <table>
                    <tr>
                        <td id="kisikisi"><a href="<?php echo base_url().'index.php/soal/cariKisi/'.$id;?>" target="kisikisiFrame"><img src="<?php echo base_url(); ?>asset/images/search-logo-md.png" width="40px"></a></td>
                        <td></td>
                        <td><input type="submit" value="CEK">
                        </td>
                    </tr>
                </table>

            </div>
        </form>
    </div>
    <div class="progress">
        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $persen ?>%">
            <span class="sr-only">20% Complete</span>
        </div>
    </div>
    <a href="<?php echo base_url() ?>index.php/Frame_Soal"><p style="font-size: 12px; text-align: center; color: #808080;">Kembali Ke Halama Utama <img src="<?php echo base_url(); ?>asset/images/Button Kembali ke laman utama.png"></p></a>
</div>
