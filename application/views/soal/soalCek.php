<script>
    $(document).ready(function () {
        document.getElementById('timer').innerHTML = 'Please wait you\'ll be redirected after <span id="countDown">3</span> seconds....';
        var count = 3;
        setInterval(function () {
            count--;
            document.getElementById('countDown').innerHTML = count;
            if (count == 0) {
                window.location = "<?php echo base_url() . 'index.php/soal/getBySoalNourut/'.$id.'/'.$nosoal; ?>";
            }
        }, 1000);
    });
</script>
<div class="panel-body panel-atas">
    <div class="judulsoal-bs">
        <table>
            <tr>
                <td><img style="margin-left: 40px; height: 40px;width: auto;" src="<?php echo $this->config->item('base_url_backend') . 'asset/uploads/' . $dataSoal->logo; ?>">
                    <p style="margin-top: 10px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php
                        echo $nosoal-1;
                        $persen = ($nosoal-1) / $jumlahSoal * 100;
                        ?>/<?php echo $jumlahSoal; ?></p></td>
                <td><p style="text-align: center;"><?php echo $dataSoal->nama_type_soal . ' ' . $dataSoal->subjek . ' ' . $dataSoal->nama . ' ' . $dataSoal->jenjang . ' Kelas ' . $dataSoal->kelas; ?> <br> <?php echo $dataSoal->nama_meta_sumber; ?></p></td>
                <td><a href="">
                                <img  style="width: 29px; height: 33px; float: right; margin-right: 40px" src="<?php echo base_url();?>asset/images/Button Laporkan.png"></a></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel-body panel-bottom">
    <div class="panel panel-default akhir-bs">
        <div class="panel-body panel-penilaian">
            <h1 style="margin: 0; text-align: center; font-weight: bold;">&nbsp;</h1> 
        </div>
        <div class="penilaian-bs">
            <h1 style="margin: 0; text-align: center; font-weight: bold;">JAWABAN <?php echo $hasilJawab; ?>!</h1>
            <div id="timer">x Secs </div>
        </div>
        <div class="panel-body panel-penilaian">
            <h1 style="margin: 0; text-align: center; font-weight: bold;">&nbsp;</h1>
        </div>
        <div class="button-bs">
            <table>
                <tr>
                    <td width="60%"></td>
                    <td><a href="<?php echo base_url() . 'index.php/soal/getBySoalNourut/'.$id.'/'.$nosoal; ?>"><input type="button" value="Lanjut"></a></td>
                </tr>
            </table>
        </div>
        <div class="panel-body panel-penilaian">
            <h1 style="margin: 0; text-align: center; font-weight: bold;">&nbsp;</h1>
        </div>
        
    </div>
    <div class="progress">
        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $persen ?>%">
            <span class="sr-only">20% Complete</span>
        </div>
    </div>
    <a href="<?php echo base_url() ?>index.php/Frame_Soal"><p style="font-size: 12px; text-align: center; color: #808080;">Kembali Ke Halama Utama <img src="<?php echo base_url(); ?>asset/images/Button Kembali ke laman utama.png"></p></a>
</div>
