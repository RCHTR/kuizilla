<link href="<?php echo base_url(); ?>asset/css/detailKisi.css" rel="stylesheet">
<style>
    #example2 {
        border: 1px solid;
        padding: 10px;
        box-shadow: 5px 10px #888888;
    }
</style>        

<style>
    .datakisi td:hover {
        background-color: #fff568;
        border-color: #18de2d;
        border-radius: 15px;
        border-style: solid;
        border-width: thin;
    }
    .menukiri ul {
        list-style-type: none;
        margin: 0;
        padding: 5px;
        color: #18de2d;
        font-size: smaller
    }

    .menukiri a{
        text-decoration: none;
        color: #18de2d;
    }

    .barisbawah div {
        border-bottom: 3px solid #ffffff;
        padding: 5px;
        font-size: smaller
    }
</style>
<div class="w3-bar-block w3-card w3-animate-left" style="display:none;position: fixed;" id="mySidebar">
    <button onclick="w3_close()" class="w3-display-topright">X</button>
    <div class="tabelmenu">
        <table>
            <tr><th> &nbsp;
                </th>
            </tr>
            <tr>
                <td><a href="<?php echo base_url() . 'index.php/Frame_Kisi'; ?>">
                        <img src="<?php echo base_url(); ?>asset/KuiziWiki Logo.png" alt="Kuizilla">
                    </a></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <div class="menukiri">
                        <ul>
                            <li><a href="<?php echo base_url(); ?>index.php/Frame_Kisi">Halaman Utama</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php/Frame_Kisi/terbaru">Terkini</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php/Frame_Kisi/kuiziKu">Kalender</a></li>
                        </ul>
                    </div>

                </td>
            </tr>
            <tr>
                <td class="barisbawah"><div>
                        KuiziWiki-Mu
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="menukiri">
                        <ul>
                            <li><a href="#">Buat Baru</a></li>
                            <?php
                            if (isset($this->session->userdata('user')->username)) {
                                echo '<li><a href="' . base_url() . 'index.php/Frame_Kisi/mypdf/' . $detailKisi->id . '">Unduh Versi PDF</a></li>';
                            }
                            ?>
                            <li><a href="http://administrator.kuizilla.com/Kisi" target="_blank">Kuizi Wiki-Ku</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="barisbawah"><div>
                        Bagikan
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="menukiri">
                        <ul>
                            <li><a href="#">Facebook</a></li>
                            <li><a href="#">Google+</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>

<div id="main">
    
    <div style="position: fixed;">
        <button id="openNav" class="w3-button w3-teal" onclick="w3_open()">&#9776;</button>

    </div>

    <div><br><br></div>

    <?php
    if (empty($detailKisi)) {
        echo '<div class="w3-container"><br><br><div style="text-align:center;">Tidak menemukan data</div></div>';
    } else {
        ?>
        <div class="w3-container">
            <h2><?php echo $detailKisi->title; ?></h2>
            <hr>
            <div class="w3-bar">
                <p>Kontributor : <?php echo $detailKisi->username; ?></p>
                <?php echo $detailKisi->tagar; ?>
            </div>

        </div>
        <div class="w3-container">
            <?php
            echo $detailKisi->content;
            ?>

            <div class="w3-card">
                <p>Tes Ilmu Pengetahuanmu </p>
                <form class="input-group" action="<?php echo base_url(); ?>index.php/Frame_Soal/getAllBy" method="post" target="soalFrame">
                    <input type="hidden" name="search" value="<?php echo $detailKisi->title; ?>"/>
                    <button type="submit">
                        Klik Disini
                    </button>

                </form>
            </div>
            <div><br></div>
        </div>
    <?php }
    ?>

</div>

<script>
    document.getElementById("main").style.marginLeft = "25%";
    document.getElementById("mySidebar").style.width = "25%";
    document.getElementById("mySidebar").style.display = "block";
    document.getElementById("openNav").style.display = 'none';

    function w3_open() {
        document.getElementById("main").style.marginLeft = "25%";
        document.getElementById("mySidebar").style.width = "25%";
        document.getElementById("mySidebar").style.display = "block";
        document.getElementById("openNav").style.display = 'none';
    }
    function w3_close() {
        document.getElementById("main").style.marginLeft = "0%";
        document.getElementById("mySidebar").style.display = "none";
        document.getElementById("openNav").style.display = "inline-block";
    }
</script>
