<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .checked {
        color: orange;
    }
</style>

<div class="panel-body panel-atas">
    <div class="judulsoal-bs">
        <table>
            <tr>
                <td><img style="margin-left: 40px; height: 40px;width: auto;" src="<?php echo $this->config->item('base_url_backend') . 'asset/uploads/' . $dataSoal->logo; ?>">
                    <p style="margin-top: 10px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php
                        echo $nosoal;
                        $persen = $nosoal / $jumlahSoal * 100;
                        ?>/<?php echo $jumlahSoal; ?></p></td>
                <td><p style="text-align: center;"><?php echo $dataSoal->nama_type_soal . ' ' . $dataSoal->subjek . ' ' . $dataSoal->nama . ' ' . $dataSoal->jenjang . ' Kelas ' . $dataSoal->kelas; ?> <br> <?php echo $dataSoal->nama_meta_sumber; ?></p></td>
                <td><a href="">
                                <img  style="width: 29px; height: 33px; float: right; margin-right: 40px" src="<?php echo base_url();?>asset/images/Button Laporkan.png"></a></td>
            
            </tr>
        </table>
    </div>
</div>
<?php
$persenSoal = $jawabanBenar / $jumlahSoal * 100;
?>

<div class="panel-body panel-bottom">
    <div class="panel panel-default akhir-bs">
        <div class="panel-body panel-penilaian">
            <h1 style="margin: 0; text-align: center; font-weight: bold;">SELESAI!</h1>
            <p style="text-align: center; font-size: 16px; color: #646464;">Jawaban Benar : <?php echo $jawabanBenar . ' dari ' . $jumlahSoal; ?>
                <br>Score: <br></p>
        </div>
        <div class="penilaian-bs">
            <h1 style="margin: 0; text-align: center; font-weight: bold;"><?php echo $nilai; ?></h1>

        </div>
        <div class="penilaian-bs">
            <span class="fa fa-star checked"></span>
            <?php
            $kata="";
            if ($persenSoal > 0) {
                echo '<span class="fa fa-star checked"></span>';
                $kata="YOU ARE GOOD";
            } else {
                echo '<span class="fa fa-star"></span>';
            }
            if ($persenSoal > 65) {
                echo '<span class="fa fa-star checked"></span>';
                $kata="YOU ARE GREAT";
            } else {
                echo '<span class="fa fa-star"></span>';
            }
            if ($persenSoal > 75) {
                $kata="YOU ARE EXCELENT";
                echo '<span class="fa fa-star checked"></span>';
            } else {
                echo '<span class="fa fa-star"></span>';
            }
            if ($persenSoal > 80) {
                $kata="YOU ARE AMAZING";
                echo '<span class="fa fa-star checked"></span>';
            } else {
                echo '<span class="fa fa-star"></span>';
            }
            if ($persenSoal > 90) {
                 $kata="YOU ARE GENIUS";
                echo '<span class="fa fa-star checked"></span>';
            } else {
                echo '<span class="fa fa-star"></span>';
            }
            ?>
        </div>
        <div class="penilaian-bs">
            <h4 style="margin: 0; text-align: center; font-weight: bold;"><?php echo $kata; ?></h4>

        </div>
        <br>
        <!--        <div class="penilaian-bs">
                        <table style="width: 100%;">
                            <tr>
                                <td><input style="background-color: #ff0000; color: #ffffff" type="submit" value="TRYOUT">
                                    <input type="submit" value="SELESAI"></td>
                            </tr>
                        </table>
                </div>-->

    </div>
    <div class="progress">
        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $persen ?>%">
            <span class="sr-only">20% Complete</span>
        </div>
    </div>
    <a href="<?php echo base_url() ?>index.php/Frame_Soal"><p style="font-size: 12px; text-align: center; color: #808080;">Kembali Ke Halama Utama <img src="<?php echo base_url(); ?>asset/images/Button Kembali ke laman utama.png"></p></a>
</div>