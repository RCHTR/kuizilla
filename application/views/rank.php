<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Kuizilla</title>

        <!-- CSS-->
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>asset/css/custom.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.dataTables.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/dataTables.bootstrap.css">
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Bree+Serif|Open+Sans" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .form-group {
                padding: 10px;
            }
            .close {
                margin: 10px;
            }
            .modal-content {
                background: #cb1cff;
                color: #fff;
            }
            .table-wrapper-scroll-y {
                display: block;
                max-height: 200px;
                overflow-y: auto;
                -ms-overflow-style: -ms-autohiding-scrollbar;
            }
            .list-group-flush:first-child .list-group-item:first-child {
                border-top: 0;
            }
            .list-group-flush .list-group-item {
                border-right: 0;
                border-left: 0;
                border-radius: 0;
            }
            .list-group-item {
                border: 1.5px solid #bbbbbb;
            }
        </style>
    </head>
    <body class="body-luar">
        <?php $this->load->view('private-header'); ?>
        <br class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row" style="text-align:center">
                            <div class="col-sm-12" style="padding-top:10px;padding-right:85px">
                                <h2>Peringkat Uji Level</h2>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1" style="overflow:auto;max-height:300px">
                                <table class="table" style="background-color:#ffdafe">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Nama</th>
                                            <th>Nilai</th>
                                            <th>Waktu</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Fulan</td>
                                            <td>100</td>
                                            <td>30:00:00</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Fulan</td>
                                            <td>100</td>
                                            <td>30:00:00</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Fulan</td>
                                            <td>100</td>
                                            <td>30:00:00</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Fulan</td>
                                            <td>100</td>
                                            <td>30:00:00</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Fulan</td>
                                            <td>100</td>
                                            <td>30:00:00</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Fulan</td>
                                            <td>100</td>
                                            <td>30:00:00</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Fulan</td>
                                            <td>100</td>
                                            <td>30:00:00</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Fulan</td>
                                            <td>100</td>
                                            <td>30:00:00</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Fulan</td>
                                            <td>100</td>
                                            <td>30:00:00</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Fulan</td>
                                            <td>100</td>
                                            <td>30:00:00</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Fulan</td>
                                            <td>100</td>
                                            <td>30:00:00</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <center><a class="btn btn-primary" href="<?php echo base_url(); ?>homepage" style="font-size:25px;border-radius:25px;;margin-top:30px">LANJUT</a></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
            });
        </script>
    </body>
</br>