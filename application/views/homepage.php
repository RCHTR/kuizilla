<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Kuizilla</title>

        <!-- CSS-->
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>asset/css/custom.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.dataTables.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/dataTables.bootstrap.css">
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Bree+Serif|Open+Sans" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .form-group {
                padding: 10px;
            }
            .close {
                margin: 10px;
            }
            .modal-content {
                background: #f9ffa4;
            }
        </style>
    </head>
    <body class="body-luar">
        <?php $this->load->view('private-header'); ?>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align:center">
                        <div class="row">
                            <div class="col-md-4 col-md-offset-2">
                                <img src="<?php echo base_url(); ?>asset/images/avatar.png" class="img-responsive ">
                            </div>
                            <div class="col-md-6" style="text-align:left">
                                <h3><?=$siswa->nama_siswa?></h3>
                                NIS : <?=$siswa->nis?><br>
                                <?=$siswa->nama_sekolah?><br>
                                <span class="glyphicon glyphicon-user">&nbsp;5 Pengikut</span>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-md-4 col-md-offset-2">
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#mathDialog"><img src="<?php echo base_url(); ?>asset/images/<?=$image[15]?>" class="img-responsive"><br>Matematika</a>
                            </div>
                            <div class="col-md-4">
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#bahasaDialog"><img src="<?php echo base_url(); ?>asset/images/<?=$image[16]?>" class="img-responsive"><br>Bahasa Indonesia</a>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-4 col-md-offset-2">
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#englishDialog"><img src="<?php echo base_url(); ?>asset/images/<?=$image[18]?>" class="img-responsive"><br>Bahasa Inggris</a>
                            </div>
                            <div class="col-md-4">
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#scienceDialog"><img src="<?php echo base_url(); ?>asset/images/<?=$image[20]?>" class="img-responsive"><br>IPA</a>
                            </div>
                        </div>
                        <div class="row" style="padding:23px">
                            <a href="#" type="button" style="font-size:25px;border-radius:17px;background-color:#cdcdcd" class="btn btn-default">Lihat Raporku</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- MATH MODAL DIALOG -->
        <div id="mathDialog" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="row" style="margin-top:35px">
                        <div class="col-md-4 col-md-offset-4">
                            <img src="<?php echo base_url(); ?>asset/images/Matematika.png" class="img-responsive">
                        </div>
                    </div>
                    <div class="row" style="text-align:center">
                        <div class="col-md-8 col-md-offset-2">
                            <h3>Uji Level Matematika Dasar</h3>
                            <p>10 Soal, durasi: 30 Menit</p>
                        </div>
                    </div>
                    <div class="row" style="text-align:center;margin:10px">
                        <div class="col-md-4 col-md-offset-4">
                            <a href="<?php echo base_url(); ?>course/math" type="button" style="font-size:25px;border-radius:25px" class="btn btn-primary">MULAI</a>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- BAHASA MODAL DIALOG -->
        <div id="bahasaDialog" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="row" style="margin-top:35px">
                        <div class="col-md-4 col-md-offset-4">
                            <img src="<?php echo base_url(); ?>asset/images/Bahasa Indonesia.png" class="img-responsive">
                        </div>
                    </div>
                    <div class="row" style="text-align:center">
                        <div class="col-md-8 col-md-offset-2">
                            <h3>Uji Level Bahasa Indonesia</h3>
                            <p>10 Soal, durasi: 30 Menit</p>
                        </div>
                    </div>
                    <div class="row" style="text-align:center;margin:10px">
                        <div class="col-md-4 col-md-offset-4">
                            <a href="<?php echo base_url(); ?>course/bahasa" type="button" class="btn btn-primary" style="font-size:25px;border-radius:25px">MULAI</a>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- ENGLISH MODAL DIALOG -->
        <div id="englishDialog" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="row" style="margin-top:35px">
                        <div class="col-md-4 col-md-offset-4">
                            <img src="<?php echo base_url(); ?>asset/images/Bahasa Inggris.png" class="img-responsive">
                        </div>
                    </div>
                    <div class="row" style="text-align:center">
                        <div class="col-md-8 col-md-offset-2">
                            <h3>Uji Level Bahasa Inggris</h3>
                            <p>10 Soal, durasi: 30 Menit</p>
                        </div>
                    </div>
                    <div class="row" style="text-align:center;margin:10px">
                        <div class="col-md-4 col-md-offset-4">
                            <a href="<?php echo base_url(); ?>course/english" type="button" style="font-size:25px;border-radius:25px" class="btn btn-primary">MULAI</a>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- SCIENCE MODAL DIALOG -->
        <div id="scienceDialog" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="row" style="margin-top:35px">
                        <div class="col-md-4 col-md-offset-4">
                            <img src="<?php echo base_url(); ?>asset/images/IPA.png" class="img-responsive">
                        </div>
                    </div>
                    <div class="row" style="text-align:center">
                        <div class="col-md-8 col-md-offset-2">
                            <h3>Uji Level IPA</h3>
                            <p>10 Soal, durasi: 30 Menit</p>
                        </div>
                    </div>
                    <div class="row" style="text-align:center;margin:10px">
                        <div class="col-md-4 col-md-offset-4">
                            <a href="<?php echo base_url(); ?>course/science" class="btn btn-primary" style="font-size:25px;border-radius:25px">MULAI</a>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {


            });

            function openDialog(course) {
                console.log(course)
            }
        </script>
    </body>
</br>