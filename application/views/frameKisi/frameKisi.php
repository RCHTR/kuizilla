<script type="text/javascript">
    $(document).ready(function () {
        $('#data').DataTable(
                {
                    searching: false,
                    ordering: false,
                    lengthMenu: [10, 20, 50],
                    lengthChange: false,
                    info: false,
                    language: {lengthMenu: "show _MENU_ records"}
                });
    });
</script>
<style>
.datakisi td:hover {
    background-color: #fff568;
    border-color: #18de2d;
    border-radius: 15px;
    border-style: solid;
    border-width: thin;
}
.menukiri ul {
    list-style-type: none;
    margin: 0;
    padding: 5px;
    color: #18de2d;
    font-size: smaller
}

.menukiri a{
    text-decoration: none;
    color: #18de2d;
}

.barisbawah div {
    border-bottom: 3px solid #ffffff;
    padding: 5px;
    font-size: smaller
}
</style>
<div class="column side">
    <div class="tabelmenu">
    <table>
        <tr><th> &nbsp;
        </th>
        </tr>
        <tr>
            <td><a href="<?php echo base_url().'index.php/Frame_Kisi';?>">
                        <img src="<?php echo base_url();?>asset/KuiziWiki Logo.png" alt="Kuizilla">
      </a></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <div class="menukiri">
                    <ul>
                        <li><a href="<?php echo base_url();?>index.php/Frame_Kisi">Halaman Utama</a></li>
                        <li><a href="<?php echo base_url();?>index.php/Frame_Kisi/terbaru">Terkini</a></li>
                        <li><a href="<?php echo base_url();?>index.php/Frame_Kisi/kuiziKu">Kalender</a></li>
                    </ul>
                </div>
                
            </td>
        </tr>
        <tr>
            <td class="barisbawah"><div>
                KuiziWiki-Mu
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="menukiri">
                    <ul>
                        <li><a href="#">Buat Baru</a></li>
                        <li><a href="http://administrator.kuizilla.com/Kisi" target="_blank">Kuizi Wiki-Ku</a></li>
                    </ul>
                </div>
            </td>
        </tr>
        <tr>
            <td class="barisbawah"><div>
                Bagikan
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="menukiri">
                    <ul>
                        <li><a href="#">Facebook</a></li>
                        <li><a href="#">Google+</a></li>
                    </ul>
                </div>
            </td>
        </tr>
    </table>
    </div>
</div>
<div class="column middle">
<?php $this->load->view($mainKisi);?>
</div>

