<div class="panel-body top-kisi-kisi">
    <div id="search"> 
        <form class="input-group stylish-input-group input-append" action="<?php echo base_url() . 'index.php/welcome/getAllByKisi'; ?>" method="post">
            <input style="height: 40px;" type="text" name="tcariKisi" id="tcariKisi" class="form-control"  placeholder="Cari KuiziWiki" required="required">
            <span class="input-group-addon">
                <button type="submit">
                    <span><img style="width: 24px; height: 25px" src="<?php echo base_url(); ?>asset/images/search-logo-md.png"></span>
                </button>  
            </span>
        </form>
    </div>
</div>
<?php
if (sizeof($dataKisi) < 1) {
    ?>
    <div class="panel-body bottom-kisi-kisi">
        <img src="<?php echo base_url(); ?>asset/images/Riwayat Pencarian.png">
        <p>Tidak Ada Riwayat Pencarian</p>
    </div>
    <div><a href="<?php echo base_url(); ?>index.php/Frame_Kisi"><p style="font-size: 12px; text-align: center; color: #808080;">Kembali Ke Halama Utama <img src="<?php echo base_url(); ?>asset/images/Button Kembali ke laman utama.png"></p></a></div>

    <?php
} else {
    ?>
    <div class="panel-body pilihankisi-kisi">
        <table id="data" class="datakisi">
            <thead>
                <tr>			
                    <th>&nbsp;</th>

                </tr>
            </thead>
            <tbody>
                <?php foreach ($dataKisi as $dt) { ?>
                    <tr><td>
                            <div class="media">
                                <div class="media-left media-middle">
                                    <a href="<?php echo base_url() . 'index.php/soal/detailKisi/' . $dt->id; ?>">
                                        <img width="50" height="auto" class="media-object" src="<?php echo $this->config->item('base_url_backend') . 'asset/uploads/' . $dt->logo; ?>" alt="...">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <a href="<?php echo base_url() . 'index.php/soal/detailKisi/' . $dt->id; ?>">
                                        <h4 style="font-weight: bold; font-size: 14px;" class="media-heading"><?php echo $dt->title; ?></h4>
                                    </a>
                                    <p style="color: #b3b3b3;"><?php echo $dt->jenjang; ?> Kelas <?php echo $dt->kelas; ?> Penulis: <?php echo $dt->penulis; ?>; <?php echo $dt->tahun_terbit; ?></p>
                                    <p>
                                        <?php
                                        $kata = substr(strip_tags($dt->content), 0, 80);
                                        $i = strrpos($kata, ' ');
                                        if ($i !== false)
                                            $kata = substr(strip_tags($dt->content), 0, $i);

                                        echo $kata . '.......';
                                        ?></p>
                                </div>
                            </div>
                        </td></tr>
                <?php } ?>
            </tbody></table>
    </div>

    <?php
}
?>
