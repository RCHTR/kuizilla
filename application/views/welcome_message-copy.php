<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Kuizilla</title>

        <!-- CSS-->
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>asset/css/custom.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.dataTables.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/dataTables.bootstrap.css">
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Bree+Serif|Open+Sans" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .profile_pic{width:100%;float:right}.img-circle.profile_img{width:10%;background:#fff;margin-left:15%;z-index:1000;position:inherit;margin-top:0px;border:1px solid rgba(52,73,94,.44);padding:1px}.profile_info{padding:25px 10px 10px;width:65%;float:left}.profile_info span{font-size:13px;line-height:30px;color:#000}.profile_info h2{font-size:14px;color:#000;margin:0;font-weight:300}.profile.img_2{text-align:center}.profile.img_2 .profile_pic{width:100%}.profile.img_2 .profile_pic .img-circle.profile_img{width:50%;margin:10px 0 0}.profile.img_2 .profile_info{padding:15px 10px 0;width:100%;margin-bottom:10px;float:left}
        </style>
    </head>
    <body class="body-luar">

        <!-- Awal Menu -->
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>asset/images/logo.png" alt="Kuizilla">
                    </a>
                </div>



                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li><a href="<?php echo base_url(); ?>">About</a></li>
                    </ul>
                    <?php
                    if (isset($this->session->userdata('user')->username)) {
                        ?>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="profile_pic">
                                <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                    <img class="img-circle profile_img" src="<?php echo base_url(); ?>asset/images/user.png" alt="Jason's Photo" />
                                    <span class="user-info">
                                        <small>Welcome, </small>
                                        <?php echo $this->session->userdata('user')->username ?>
                                    </span>

                                    <i class="ace-icon fa fa-caret-down"></i>
                                </a>

                                <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                    <li>
                                        <a href="<?php echo $this->config->item('base_url_backend'); ?>auth/change_password">
                                            <i class="ace-icon fa fa-cog"></i>
                                            Change Password
                                        </a>
                                    </li>
                                    <!--
                                                    <li>
                                                        <a href="profile.html">
                                                            <i class="ace-icon fa fa-user"></i>
                                                            Profile
                                                        </a>
                                                    </li>-->

                                    <li class="divider"></li>

                                    <li>
                                        <a href="<?php echo $this->config->item('base_url_backend'); ?>auth/logout">
                                            <i class="ace-icon fa fa-power-off"></i>
                                            Logout
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>        
                        <?php
                    } else {
                        ?>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="http://administrator.kuizilla.com"><button style="padding: 2px 8px;"  type="button" class="btn btn-light login-btn">Login</button></a></li>
                        </ul>           
                        <?php
                    }
                    ?>
                    <!--                    -->

                </div>
                <!--/.nav-collapse -->
            </div>
            <!--/.container-fluid -->
        </nav>
        <!-- Akhir menu -->
        <?php $this->load->view($main); ?>

        <script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
    </body>
</html>