<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Kuizilla</title>

        <!-- CSS-->
        <link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>asset/css/custom.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.dataTables.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/dataTables.bootstrap.css">
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Bree+Serif|Open+Sans" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .form-group {
                padding: 10px;
            }
            .close {
                margin: 10px;
            }
            .modal-content {
                background: #ffffff;
            }
            .btn-default {
                background: #ccc;
            }
            #progressBar {
                border-radius: 10px;
                width: 100%;
                margin-bottom: 10px;
                height: 29px;
                background-color: #9dc0ec;
            }

            #progressBar div {
                height: 100%;
                text-align: right;
                padding: 0 10px;
                line-height: 29px; /* same as #progressBar height if we want text middle aligned */
                width: 0;
                background-color: #5bb3c8;
                box-sizing: border-box;
                border-radius: 10px;
                color: #fff;
            }

            .carousel-control.left {
                background-image: none !important;
                width: 30px;
                height: 30px;
                top: 365px !important;
                position: absolute;
                right: 0;
                z-index: 15;
                margin-left: 20px;
            }
            .carousel-control.right {
                background-image: none !important;
                width: 30px;
                height: 30px;
                top: 365px !important;
                position: absolute;
                right: 0;
                z-index: 15;
                margin-right: 20px;
            }
            .carousel-control .glyphicon-chevron-left, .carousel-control .glyphicon-chevron-right, .carousel-control .icon-next, .carousel-control .icon-prev {
                color: grey;
            }
            .carousel-indicators {
                bottom: -65px !important;
                position: absolute;
                right: 0;
                z-index: 15;
                width: 60%;
                margin-right: 20px;
                list-style: none;
                text-align: center;
            }
            .carousel-indicators .active {
                width: 24px;
                height: 24px;
                background-color: #8a0091;
            }
            .carousel-indicators .dijawab {
                width: 24px;
                height: 24px;
                background-color: #ffa500;
            }
            .carousel-indicators li {
                width: 24px;
                height: 24px;
                background-color: #f2f3ff;
            }
            .carousel-indicators li {
                border: 3px solid #fff;
                border-radius: 17px;
            }

            .carousel-inner {
                margin-bottom:50px;
                max-height: 345px;
                overflow: auto;
            }

            #myCarousel {
                height: 345px;
                max-height: 345px;
                font-size: 16px !important;
            }

            label {
                position: inherit;
                top: 0px;
                color: #000;
                font-size: 16px;
                /* font-weight: 300; */
                transition: 0.2s ease all;
                -moz-transition: 0.2s ease all;
                /* -webkit-transition: 0.2s ease all; */
            }

            .carousel-control .glyphicon-chevron-left, .carousel-control .glyphicon-chevron-right {
                color: #8a0091;
            }
        </style>
    </head>
    <body class="body-luar">
        <?php $data['is_exam'] = true;$this->load->view('private-header',$data); ?>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div id="progressBar">
                    <div class="bar"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <?php
                                $jumlah_soal = count($dataSoal);
                            ?>
                            <div class="slider" id="main-slider">
                                <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
                                    <!-- Indicators -->
                                    <ol class="carousel-indicators">
                                        <?php
                                            for ($i=0; $i < $jumlah_soal; $i++) { 
                                        ?>
                                        <li data-target="#myCarousel" data-slide-to="<?=$i?>" <?=($i==0)? 'class="active"':''?>></li>
                                        <?php
                                            }
                                        ?>
                                    </ol>

                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">
                                        <!-- <form id="form-exam"> -->
                                        <?php
                                            foreach ($dataSoal as $key => $value) {
                                        ?>
                                        <div class="item <?=($key==0)?'active':''?>">
                                            <div class="col-md-10 col-md-offset-1">
                                                <center><?= ($key+1)."/".$jumlah_soal ?></center><br>
                                                <input type="hidden" class="exam_detail" value="<?= $value->id ?>">
                                                <?= $value->pertanyaan ?><br>
                                                <div class="radio">
                                                    <label><input class="test" type="radio" value="<?= $value->id ?>_A" name="optradio[<?=$key?>]"><?= $value->pilihan_a ?></label>
                                                </div>
                                                <div class="radio">
                                                    <label><input class="test" type="radio" value="<?= $value->id ?>_B" name="optradio[<?= $key ?>]"><?= $value->pilihan_b ?></label>
                                                </div>
                                                <div class="radio">
                                                    <label><input class="test" type="radio" value="<?= $value->id ?>_C" name="optradio[<?= $key ?>]"><?= $value->pilihan_c ?></label>
                                                </div>
                                                <div class="radio">
                                                    <label><input class="test" type="radio" value="<?= $value->id ?>_D" name="optradio[<?= $key ?>]"><?= $value->pilihan_d ?></label>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                            }
                                        ?>
                                        <!-- </form> -->
                                    </div>

                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <center><a class="btn btn-primary" href="javasctipr:void(0);" data-toggle="modal" data-target="#mathDialog" style="font-size:25px;border-radius:25px;background-color: #8b008b;border-color: #8b008b;margin-top:30px">SELESAI</a></center>
        </div>

        <!-- MODAL DIALOG FINISH -->
        <div id="mathDialog" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="row" style="margin-top:35px">
                        <div class="col-md-4 col-md-offset-4">
                            <img src="<?php echo base_url(); ?>asset/images/Maskot Login Berhasil.png" class="img-responsive">
                        </div>
                    </div>
                    <div class="row" style="text-align:center">
                        <div class="col-md-8 col-md-offset-2">
                            <h3>Apakah kamu yakin menyelesaikan uiji level ini?</h3>
                        </div>
                    </div>
                    <div class="row" style="text-align:center;margin:10px">
                        <div class="col-md-6">
                            <a href="javasctipr:void(0);" data-dismiss="modal" type="button" style="font-size:25px;border-radius:25px;width:115px;" class="btn btn-default">Kembali</a>
                        </div>
                        <div class="col-md-6">
                            <a href="#" onClick="selesai()" type="button" style="font-size:25px;border-radius:25px;width:115px;" class="btn btn-default">Ya</a>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                progress(1800, 1800, $('#progressBar'));

                $('.test').change(function() {
                    console.log('test');
                    console.log($('#myCarousel .active'))
                })
            });
            function progress(timeleft, timetotal, $element) {
                var progressBarWidth = timeleft * $element.width() / timetotal;
                $element.find('div').animate({ width: progressBarWidth }, 500).html(Math.floor(timeleft/60) + ":"+ timeleft%60);
                if(timeleft > 0) {
                    setTimeout(function() {
                        progress(timeleft - 1, timetotal, $element);
                    }, 1000);
                }
            }

            function selesai() {
                var examDetail = [] 
                var examCollect = []
                var examCollectId = []
                $(".item .test:checked").each(function() {
                    examCollect.push($(this).val())
                });

                $(".item .exam_detail").each(function() {
                    examCollectId.push($(this).val())
                })

                $.ajax({
                    method: "POST",
                    url: "<?= base_url('course/store') ?>",
                    data: {examCollect : examCollect, examCollectId: examCollectId, mapel_id: <?=$soal?>, bab_id: <?=$id_bab?>, level_id: <?=$id_level?>}
                })
                .done(function( response ) {
                    var res = JSON.parse(response)
                    console.log(res)
                    if(res.status){
                        window.location = '<?php echo base_url(); ?>course/result/'+res.data.resultId;
                    }
                });

            }
        </script>
    </body>
</br>