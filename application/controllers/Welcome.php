<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('Login_auth');
        $this->load->model('Guru_model');
    }

    public function index() {
        if (!$this->session->userdata('login')) {
            $this->load->view('welcome_message');
        } else {
            redirect('/homepage', 'refresh');
        }
    }
    public function v_login_guru() {
        $this->load->view('guru/welcome_message_guru');
        
        // $where = array(
        //     'nip' => '007',
        //     'm_pegawai_sekolah.telepon' => '0812424252'
        // );
        // $dataGuru = $this->Guru_model->get($where);
        // print_r($dataGuru);
        // if (!$this->session->userdata('login')) {
        //     $this->load->view('guru/welcome_message_guru');
        // } else {
        //     redirect('/homepage', 'refresh');
        // }
    }
}
