<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model('Siswa_model');
        $this->load->model('Guru_model');
        $this->load->library('Login_auth');
    }

    public function index() {
        $this->login_auth->set_user_info();
    }

    public function login() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $type = $this->input->post('type');
        $where = array(
            'nis' => $username,
            'password' => $password
        );
        // if ($type == 'murid') {
            // LOGIN MURID
            $cek = $this->Siswa_model->login($where);    
        // } else {
            // $cek = $this->m_login->cek_login("admin", $where)->num_rows();
        // }
        header('Content-Type: application/json');
        if ($cek) {
            $dataSiswa = $this->Siswa_model->get($where);
            $data_session = array(
                'nama' => $username,
                'login' => true,
                'ip' => $this->input->ip_address(),
                'type' => $type,
                'siswa' => $dataSiswa
            );

            $this->session->set_userdata($data_session);
            echo json_encode(array('status' => true, 'message' => 'berhasil', 'data' => $dataSiswa));
        } else {
            echo json_encode(array('status' => false, 'message' => 'maaf password atau username anda salah'));
        }
    }

    public function logout () {
        $this->session->sess_destroy();
        redirect('/welcome', 'refresh');
    }
    
    public function getBy($id=0) {
        $data['dataSoal'] = $this->Soal_model->get_allByMapelID($id);            
        $data['dataMapel'] = $this->Mapel_model->get_all();            
        $data['dataKisi'] = $this->Kisi_model->get_allJoin();            
        $data['soal'] = 'soal';
        $data['kisikisi'] = 'kisikisi';
        $data['main'] = 'home';
        $this->load->view('welcome_message', $data);
    }
    public function getAllBy($text='') {
        $data['dataSoal'] = $this->Soal_model->get_allBy($text);            
        $data['dataMapel'] = $this->Mapel_model->get_all();            
        $data['dataKisi'] = $this->Kisi_model->get_allJoin();            
        $data['main'] = 'home';
        $data['soal'] = 'soal';
        $data['kisikisi'] = 'kisikisi';
        $this->load->view('welcome_message', $data);
    }
    
    public function getAllByKisi() {
        $text= $this->input->post('tcariKisi');
        $data['dataKisi'] = $this->Kisi_model->get_allBy($text);            
        $data['kisikisi'] = 'kisikisi';
        $data['main'] = 'frameKisi/frameKisi';
        $data['mainKisi'] = 'frameKisi/daftarKisi';
        $this->load->view('frameKisi/home', $data);
    }
    public function getOrder($text='') {
        $data['dataSoal'] = $this->Soal_model->get_allOrder($text);            
        $data['dataMapel'] = $this->Mapel_model->get_all();            
        $data['dataKisi'] = $this->Kisi_model->get_allJoin();            
        $data['main'] = 'home';
        $data['soal'] = 'soal';
        $data['kisikisi'] = 'kisikisi';
        $this->load->view('welcome_message', $data);
    }

    public function login_guru() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $type = $this->input->post('type');
        $where = array(
            'nip' => $username,
            'm_pegawai_sekolah.telepon' => $password
        );
        // if ($type == 'murid') {
            // LOGIN MURID
        $cek = $this->Guru_model->login($where);
        // } else {
            // $cek = $this->m_login->cek_login("admin", $where)->num_rows();
        // }
        header('Content-Type: application/json');
        if ($cek) {
            $dataGuru = $this->Guru_model->get($where);
            $data_session = array(
                'nama' => $username,
                'login' => true,
                'ip' => $this->input->ip_address(),
                'type' => $type,
                'guru' => $dataGuru
            );
            $this->session->set_userdata($data_session);
            echo json_encode(array('status' => true, 'message' => 'berhasil', 'data' => $dataGuru));
        } else {
            echo json_encode(array('status' => false, 'message' => 'maaf password atau username anda salah'));
        }
    }

}
