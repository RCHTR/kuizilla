<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        if ($this->session->userdata('login')) {
            $this->load->helper('url');
            $this->load->library('Login_auth');
            $this->load->model('Uji_model');
            $this->load->model('Mapel_model');
            $this->load->model('Guru_model');
        } else {
            redirect('/welcome', 'refresh');
        }
    }

    public function index() {
        if($this->session->userdata('type')=='guru'){
            redirect('/Homepage/index_guru');
        }else{
            $data['siswa'] = $this->session->userdata('siswa')[0];
            $data['levelSiswa'] = $this->Uji_model->getLevelSiswa($data['siswa']->id);
            // echo "<pre>";
            // print_r($data);
            // echo "</pre>";
            $data['image'][15] = (count($data['levelSiswa'][15]) !== 0) ? 'Matematika.png' : 'Matematika Locked.png' ;
            $data['image'][16] = (count($data['levelSiswa'][16]) !== 0) ? 'Bahasa Indonesia.png' : 'Bahasa Indonesia Locked.png' ;
            $data['image'][18] = (count($data['levelSiswa'][18]) !== 0) ? 'Bahasa Inggris.png' : 'Bahasa Inggris Locked.png' ;
            $data['image'][20] = (count($data['levelSiswa'][20]) !== 0) ? 'IPA.png' : 'IPA Locked.png' ;
            $this->load->view('homepage', $data);
        }
        
    }

    public function index_guru() {
        $data['guru'] = $this->session->userdata('guru')[0];
        $this->load->view('guru/homepage_guru', $data);
    }

    public function list_kelas_guru(){
        $data['guru'] = $this->session->userdata('guru')[0];
        $data['kelas_guru'] = $this->Guru_model->get_kelas_guru($this->session->userdata('guru')[0]->id);
        $data['mapel'] = $this->Mapel_model->get_all();
        $this->load->view('guru/list_kelas', $data);
    }

    public function targetHarian () {
        $data['siswa'] = $this->session->userdata('siswa')[0];
        $this->load->view('target-harian', $data);
    }
    public function uji_guru($kelas){
        $data['kelas'] = $this->Guru_model->get_kelas_guru_byidkelas($kelas);
        // $data['kelas'] = str_replace("%20"," ",$kelas);
        $data['guru'] = $this->session->userdata('guru')[0];
        $data['bab'] = $this->Mapel_model->get_all_bab();
        $data['mapel'] = $this->Mapel_model->get_all();
        $this->load->view('guru/uji_kelas', $data);
    }
    // public function create_uji($)
    public function uji_level($kelas,$mapel){
        $data['kelas'] = $this->Guru_model->get_kelas_guru_byidkelas($kelas);
        // $data['kelas'] = str_replace("%20"," ",$kelas);
        $data['mapel_uji'] = $this->Mapel_model->get_by_id($mapel);
        // $data['mapel_uji'] = str_replace("%20"," ",$mapel);
        $data['guru'] = $this->session->userdata('guru')[0];
        $data['Kode_rand'] = $this->generateRandomString(8);
        $this->load->view('guru/v_uji_level', $data);
    }
    public function hasil_uji($kelas,$mapel){
        $data['kelas'] = $this->Guru_model->get_kelas_guru_byidkelas($kelas);
        // $data['kelas'] = str_replace("%20"," ",$kelas);
        $data['mapel_uji'] = $this->Mapel_model->get_by_id($mapel);
        // $data['mapel_uji'] = str_replace("%20"," ",$mapel);
        $data['guru'] = $this->session->userdata('guru')[0];
        $data['Kode_rand'] = $this->generateRandomString(8);
        $this->load->view('guru/nilai_uji', $data);
    }
    function generateRandomString($length) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
