<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Frame_Kisi extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model('Mapel_model');
        $this->load->model('Soal_model');
        $this->load->model('Kisi_model');
        $this->load->library('pdf');
    }

    public function index() {
        $data['dataKisi'] = $this->Kisi_model->get_allJoin();

        $data['main'] = 'frameKisi/frameKisi';
        $data['mainKisi'] = 'frameKisi/daftarKisi';
        $this->load->view('frameKisi/home', $data);
    }

    function mypdf($id) {
        $data['dataKisi'] = $this->Kisi_model->get_allJoin();
        $data['detailKisi'] = $this->Kisi_model->get_by_id($id);        
        $this->load->library('pdf');
        $this->pdf->load_view('frameKisi/example_to_pdf',$data);
        $this->pdf->render();
        $this->pdf->stream("welcome.pdf");
    }

    function mypdf2($id) {
        $data['dataKisi'] = $this->Kisi_model->get_allJoin();
        $data['detailKisi'] = $this->Kisi_model->get_by_id($id);        
        $this->load->view('frameKisi/example_to_pdf',$data);
        
    }
    public function kuiziKu() {
        $data['dataKisi'] = $this->Kisi_model->get_allJoin();

        $data['main'] = 'frameKisi/frameKisi';
        $data['mainKisi'] = 'frameKisi/pilihTanggal';
        $this->load->view('frameKisi/home', $data);
    }

    public function getByDate() {
        $date = $this->input->post('date');
        $data['dataKisi'] = $this->Kisi_model->get_allJoinByDate($date);

        $data['main'] = 'frameKisi/frameKisi';
        $data['mainKisi'] = 'frameKisi/daftarKisi';
        $this->load->view('frameKisi/home', $data);
    }

    public function terbaru() {
        $data['dataKisi'] = $this->Kisi_model->get_allJoinLimit(3);

        $data['main'] = 'frameKisi/frameKisi';
        $data['mainKisi'] = 'frameKisi/daftarKisi';
        $this->load->view('frameKisi/home', $data);
    }

}
