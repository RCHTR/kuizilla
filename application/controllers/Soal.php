<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Soal extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        if ($this->session->userdata('login')) {
            $this->load->model('Mapel_model');
            $this->load->model('Soal_model');
            $this->load->model('Kisi_model');
        } else {
            redirect('/welcome', 'refresh');
        }
    }

    public function index() {
        $data['dataSoal'] = $this->Soal_model->get_allJoin();
        $data['dataMapel'] = $this->Mapel_model->get_all();
        $data['dataKisi'] = $this->Kisi_model->get_allJoin();

        $data['main'] = 'frameSoal/frameSoal';
        $this->load->view('frameSoal/home', $data);
    }

    public function getBy($id = 0) {
        $data['dataSoal'] = $this->Soal_model->get_allJoinById($id);
        $data['jumlahSoal'] = sizeof($this->Soal_model->getdetail_by_id($id));
        $data['dataSoalDetail'] = $this->Soal_model->getdetail_by_nourut_id(1, $id);
        $data['dataKisi'] = $this->Kisi_model->get_allJoin();

        if (sizeof($data['dataSoalDetail']) < 1) {
            $data['main'] = 'soal/soalDetailEmpty';
        } else {
            $data['main'] = 'soal/soalDetail';
        }
        $data['id'] = $id;
        $data['nosoal'] = 1;
        $this->load->view('frameSoal/home', $data);
    }

    public function getBySoalNourut($id = 0, $nourut = 0) {
        $data['jumlahSoal'] = sizeof($this->Soal_model->getdetail_by_id($id));
        $data['dataKisi'] = $this->Kisi_model->get_allJoin();
        $hasil = $this->Soal_model->getdetail_by_nourut_id($nourut - 1, $id);
        if ($nourut == 1) {
            $this->session->unset_userdata('jawabanBenar');
            $this->session->set_userdata('jawabanBenar', 0);
            $this->session->unset_userdata('nilai');
            $this->session->set_userdata('nilai', 0);
        }
        if ($nourut > $data['jumlahSoal']) {
            $jum = $this->session->userdata('jawabanBenar');
            $this->session->unset_userdata('jawabanBenar');
            //redirect('/soal/hasil/'.$jum.'/'.$id);
            $nil = $this->session->userdata('nilai');
            $this->session->unset_userdata('nilai');
            $this->hasil($jum, $id, $nil);
        } else {
            $data['dataSoal'] = $this->Soal_model->get_allJoinById($id);
            $data['dataSoalDetail'] = $this->Soal_model->getdetail_by_nourut_id($nourut, $id);
            $data['main'] = 'soal/soalDetail';
            $data['id'] = $id;
            $data['nosoal'] = $nourut;

            $this->load->view('frameSoal/home', $data);
        }
    }

    public function soalCek() {
        $this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');
        $id = $this->input->post("id");
        $nourut = $this->input->post("nosoal");

        $this->form_validation->set_rules('jawaban', 'jawaban', 'required');
        if ($this->form_validation->run() == FALSE) {
            redirect('/soal/getBySoalNoUrut/' . $id . '/' . $nourut . '/');
        } else {
            $jawaban = $this->input->post("jawaban");
            $data['jumlahSoal'] = sizeof($this->Soal_model->getdetail_by_id($id));
            $hasil = $this->Soal_model->getdetail_by_nourut_id($nourut, $id);
            if ($nourut == 1) {
                $this->session->unset_userdata('jawabanBenar');
                $this->session->set_userdata('jawabanBenar', 0);
                $this->session->unset_userdata('nilai');
                $this->session->set_userdata('nilai', 0);
            }
            if ($hasil->jawaban == $jawaban) {
                $i = $this->session->userdata('jawabanBenar');
                $i = $i + 1;
                $this->session->set_userdata('jawabanBenar', $i);
                $i2 = $this->session->userdata('nilai');
                $i2 = $i2 + $hasil->bobot;
                $this->session->set_userdata('nilai', $i2);
                $data['hasilJawab'] = "BENAR";
            } else {
                $data['hasilJawab'] = "KURANG TEPAT";
                $i = $this->session->userdata('nilai');
                $i = $i - $hasil->bobot_salah;
                $this->session->set_userdata('nilai', $i);
            }

            if ($nourut > $data['jumlahSoal']) {
                $jum = $this->session->userdata('jawabanBenar');
                $this->session->unset_userdata('jawabanBenar');
                $nil = $this->session->userdata('nilai');
                $this->session->unset_userdata('nilai');
                //redirect('/soal/hasil/'.$jum.'/'.$id);
                $this->hasil($jum, $id, $nil);
            } else {
                $data['dataSoal'] = $this->Soal_model->get_allJoinById($id);
                $data['main'] = 'soal/soalCek';
                $data['id'] = $id;
                $data['nosoal'] = $nourut + 1;

                $this->load->view('frameSoal/home', $data);
            }
        }
    }

    public function hasil($benar = 0, $id = 0, $nil = 0) {
        $data['dataKisi'] = $this->Kisi_model->get_allJoin();
        $data['jumlahSoal'] = sizeof($this->Soal_model->getdetail_by_id($id));
        $data['dataSoal'] = $this->Soal_model->get_allJoinById($id);
        $bintang = $persenSoal = $benar / $data['jumlahSoal'] * 100;
        if ($bintang > $data['dataSoal']->skor_tertinggi) {
            $update = array('skor_tertinggi' => $bintang);
            $this->Soal_model->updateBintang($update, $id);
        }
        $data['main'] = 'soal/selesai';
        $data['id'] = $id;
        $data['nilai'] = $nil;
        $data['jawabanBenar'] = $benar;
        $data['nosoal'] = $data['jumlahSoal'];
        $this->load->view('frameSoal/home', $data);
    }

    public function detailKisi($id = 0) {
        $data['dataKisi'] = $this->Kisi_model->get_allJoin();
        $data['detailKisi'] = $this->Kisi_model->get_by_id($id);
        $data['main'] = 'soal/kisikisiDetail';
        $this->load->view('frameSoal/home', $data);
    }

    public function cariKisi($soalid = 0) {
        $cari = $this->Soal_model->get_by_id($soalid)->tagar;
        $a = $this->Kisi_model->get_allBy($cari);
        $data['judul'] = $cari;
        foreach ($a as $dt) {
            $data['detailKisi'] = $dt;
        }

        $data['main'] = 'soal/kisikisiDetail';
        $this->load->view('frameSoal/home', $data);
    }

}
