<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Journey extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        if ($this->session->userdata('login')) {
            $this->load->helper('url');
            $this->load->library('Login_auth');
            $this->load->model('Bab_model');
            $this->load->model('Mapel_model');
            $this->load->model('Siswa_model');
        } else {
            redirect('/welcome', 'refresh');
        }
    }

    public function index() {
        $auth = $this->login_auth->set_user_info();
        $mapel = ($this->input->get('mp') !== null) ? $this->input->get('mp') : 15;
        // if ($auth !== NULL) {
            switch ($mapel) {
                case 16:
                    $prev = 15;
                    $next = 18;
                    break;

                case 18:
                    $prev = 16;
                    $next = 20;
                    break;

                case 20:
                    $prev = 18;
                    $next = 15;
                    break;
                
                default:
                    $prev = 20;
                    $next = 16;
                    break;
            }

            $data['siswa'] = $this->session->userdata('siswa')[0];
            $get_bab = $this->Bab_model->get_BabLevel($mapel);
            $data['mapel'] = $this->Mapel_model->get_by_id($mapel);
            $data['next'] = $next;
            $data['prev'] = $prev;
            error_reporting(0);
            if($get_bab!= NULL){
                foreach ($get_bab as $key => $value) {
                    $level['bab_id'] = $value->bab_id;
                    $level['nama'] = $value->nama;
                    $level['bab'] = $value->bab;
                    $level_1 = $this->Siswa_model->get_level_by_bab_mapel_user(2,$mapel,$value->bab_id,1);
                    if($level_1->num_rows() > 0){
                        $level['level_1'] = $level_1->row()->bintang;
                    }else{
                        $level['level_1'] = 0;
                    }
                    $level_2 = $this->Siswa_model->get_level_by_bab_mapel_user(2,$mapel,$value->bab_id,2);
                    if($level_2->num_rows() > 0){
                        $level['level_2'] = $level_2->row()->bintang;
                    }else{
                        $level['level_2'] = 0;
                    }
                    $level_3 = $this->Siswa_model->get_level_by_bab_mapel_user(2,$mapel,$value->bab_id,3);
                    if($level_3->num_rows() > 0){
                        $level['level_3'] = $level_3->row()->bintang;
                    }else{
                        $level['level_3'] = 0;
                    }
                    $level_4 = $this->Siswa_model->get_level_by_bab_mapel_user(2,$mapel,$value->bab_id,4);
                    if($level_4->num_rows() > 0){
                        $level['level_4'] = $level_4->row()->bintang;
                    }else{
                        $level['level_4'] = 0;
                    }
                    $data['bab'][] = (object) $level;
                }
            }
            // $data['bab'] = $bab;
            $this->load->view('journey', $data);
        // } else {
        //     redirect('/welcome', 'refresh');
        // }
    }
}
