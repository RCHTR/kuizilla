<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model('Mapel_model');
        $this->load->model('Soal_model');
        $this->load->model('Kisi_model');
        $this->load->library('Login_auth');
    }

    public function index() {
        $this->login_auth->set_user_info();
        $data['dataSoal'] = $this->Soal_model->get_allJoin();            
        $data['dataMapel'] = $this->Mapel_model->get_all();            
        $data['dataKisi'] = $this->Kisi_model->get_allJoin();            
        $data['main'] = 'home';
        $data['soal'] = 'soal';
        $data['kisikisi'] = 'kisikisi';
        $this->load->view('welcome_message', $data);
    }
    
    public function getBy($id=0) {
        $data['dataSoal'] = $this->Soal_model->get_allByMapelID($id);            
        $data['dataMapel'] = $this->Mapel_model->get_all();            
        $data['dataKisi'] = $this->Kisi_model->get_allJoin();            
        $data['soal'] = 'soal';
        $data['kisikisi'] = 'kisikisi';
        $data['main'] = 'home';
        $this->load->view('welcome_message', $data);
    }
    public function getAllBy($text='') {
        $data['dataSoal'] = $this->Soal_model->get_allBy($text);            
        $data['dataMapel'] = $this->Mapel_model->get_all();            
        $data['dataKisi'] = $this->Kisi_model->get_allJoin();            
        $data['main'] = 'home';
        $data['soal'] = 'soal';
        $data['kisikisi'] = 'kisikisi';
        $this->load->view('welcome_message', $data);
    }
    
    public function getAllByKisi() {
        $text= $this->input->post('tcariKisi');
        $data['dataKisi'] = $this->Kisi_model->get_allBy($text);            
        $data['kisikisi'] = 'kisikisi';
        $data['main'] = 'frameKisi/frameKisi';
        $data['mainKisi'] = 'frameKisi/daftarKisi';
        $this->load->view('frameKisi/home', $data);
    }
    public function getOrder($text='') {
        $data['dataSoal'] = $this->Soal_model->get_allOrder($text);            
        $data['dataMapel'] = $this->Mapel_model->get_all();            
        $data['dataKisi'] = $this->Kisi_model->get_allJoin();            
        $data['main'] = 'home';
        $data['soal'] = 'soal';
        $data['kisikisi'] = 'kisikisi';
        $this->load->view('welcome_message', $data);
    }

}
