<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        if ($this->session->userdata('login')) {
            $this->load->helper('url');
            $this->load->library('Login_auth');
            $this->load->model('Siswa_model');
            $this->load->model('Soal_model');
            $this->load->model('Uji_model');
            $this->load->model('Bab_model');
        } else {
            redirect('/welcome', 'refresh');
        }
    }

    public function index() {
        $auth = $this->login_auth->set_user_info();
        // if ($auth !== NULL) {
            $this->load->view('homepage');
        // } else {
        //     redirect('/welcome', 'refresh');
        // }
    }

    public function math() {
        $this->load->view('math');
    }

    public function bahasa() {
        $this->load->view('bahasa');
    }

    public function science() {
        $this->load->view('science');
    }

    public function english() {
        $this->load->view('english');
    }

    public function participants() {
        $data['soal'] = $this->input->get('t');
        $data['siswa'] = $this->session->userdata('siswa')[0];
        $data['participant'] = $this->Siswa_model->get(array("kelas_id" => $data['siswa']->id_kelas));
        switch ($data['soal']) {
            case 15:
                $data['mapel'] = 'Matematika Dasar';
                $data['icon'] = base_url('asset/images/Matematika.png');
                break;
            case 16:
                $data['mapel'] = 'Bahasa Indonesia';
                $data['icon'] = base_url('asset/images/Bahasa Indonesia.png');
                break;
            case 18:
                $data['mapel'] = 'IPA';
                $data['icon'] = base_url('asset/images/IPA.png');
                break;
            case 20:
                $data['mapel'] = 'Bahasa Inggris';
                $data['icon'] = base_url('asset/images/Bahasa Inggris.png');
                break;
            
            default:
                # code...
                break;
        }
        $this->load->view('participants',$data);
    }

    public function exam() {
        $data['soal'] = $this->input->get('t');
        $get_level = $this->Siswa_model->get_uji_by_siswa_mapel(2,$this->input->get('t'));
        if($get_level->num_rows() < 1){
            $data['dataSoal'] = $this->Soal_model->getsoal_limit(10,7,$this->input->get('t'));
            $data['id_bab'] = 0;
            $data['id_level'] = 0;
        }else{
            foreach($this->Siswa_model->get_bab_tertinggi(2)->result() as $key) {
                $bab_tertinggi = $key->id_bab;
            }
            foreach($this->Siswa_model->get_level_tertinggi(2,$bab_tertinggi)->result() as $key) {
                $level_tertinggi = $key->id_level;
                $id_leveling = $key->id;
                $bintang = $key->bintang;
            }
            if($level_tertinggi == 4 && $bintang > 2){
                $bab_tertinggi = $bab_tertinggi + 1;
                $level_tertinggi = 1;
            }elseif($bintang > 2){
                $level_tertinggi  = $level_tertinggi + 1;
            }else{
                $bab_tertinggi = $bab_tertinggi;
                $level_tertinggi = $level_tertinggi;
            }
            $data['dataSoal'] = $this->Soal_model->getsoal_bab_level_limit(10,7,$this->input->get('t'),$bab_tertinggi,$level_tertinggi);
        }
        $data['id_bab'] = $bab_tertinggi;
        $data['id_level'] = $level_tertinggi;
        // echo "<pre>";
        // print_r($data['dataSoal']);
        // echo "</pre>";
        $this->load->view('exam',$data);
    }

    public function store() {
        $siswa = $this->session->userdata('siswa')[0];

        $examCollect = array();
        foreach ($_POST['examCollect'] as $value) {
            $explodeAnswer = explode('_',$value);
            $examCollect[$explodeAnswer[0]] = $explodeAnswer[1];
        }

        $nilai = 0;
        $hasil = array();
        foreach ($_POST['examCollectId'] as $value) {
            $jawaban = '';
            if(array_key_exists($value,$examCollect)) {
                $jawaban = $examCollect[$value];
            }
            
            if (count($this->Soal_model->getJawaban($value, $jawaban)->result()) != 0 ) {
                $nilai++;
                $hasil[$value] = true;
            } else {
                $hasil[$value] = false;
            }
        }

        $data['master'] = array(
            'id_siswa' => $siswa->id,
            'nama' => $siswa->nama_siswa,
            'nilai' => ($nilai * 10),
            'nis' => $siswa->nis,
            'tanggal' => date('Y-m-d'),
            'type_soal_id' => 1,
            'mapel_id' => $_POST['mapel_id']
        );
        if($nilai * 10 >=81){
            $bintang = 3;
        }elseif($nilai * 10 >=61 && $nilai * 10 <=80){
            $bintang = 2;
        }elseif($nilai * 10 >=41 && $nilai * 10 <=40){
            $bintang = 1;
        }else{
            $bintang = 0;
        }
        $data['detail'] = $hasil;
        $storeUji = $this->Uji_model->store($data);
        $leveling = array(
            'id_siswa' => $siswa->id,
            'id_bab' => $_POST['bab_id'],
            'id_level' => $_POST['level_id'],
            'bintang' => $bintang,
            'id_uji' => $storeUji,
            'mapel_id' => $_POST['mapel_id'],
        );
        if($_POST['level_id'] != 0 && $_POST['bab_id'] != 0){
            $this->Siswa_model->insert_leveling($leveling);
        }
        
        $response = array(
            'nilai' => ($nilai * 10), 
            'hasil' => $hasil,
            'resultId' => $storeUji
        );
        foreach($this->Siswa_model->get_bab_tertinggi(2)->result() as $key) {
            $bab_tertinggi = $key->id_bab;
        }
        foreach($this->Siswa_model->get_level_tertinggi(2,$bab_tertinggi)->result() as $key) {
            $level_tertinggi = $key->id_level;
            $id_leveling = $key->id;
            $bintang = $key->bintang;
        }
        echo json_encode(array('status' => true, 'data' => $response));
    }

    public function result($id) {
        $data['result'] = $this->Uji_model->getUjiResult($id)[0];
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        $this->load->view('examResult',$data);
    }

    public function report() {
        $this->load->view('courseReport');
    }

    public function rank() {
        $this->load->view('rank');
    }
    public function getlevel(){
        print_r($this->Siswa_model->get_current_leveling(2)->result());
    }
    
    public function cek_bab_tertinggi(){
        foreach($this->Siswa_model->get_bab_tertinggi(2)->result() as $key) {
            $bab_tertinggi = $key->id_bab;
        }
        foreach($this->Siswa_model->get_level_tertinggi(2,$bab_tertinggi)->result() as $key) {
            $level_tertinggi = $key->id_level;
            $id_leveling = $key->id;
            $bintang = $key->bintang;
        }
        if($level_tertinggi == 4 && $bintang > 2){
            $bab_tertinggi = $bab_tertinggi + 1;
            $level_tertinggi = 1;
        }elseif($bintang > 2){
            $level_tertinggi  = $level_tertinggi + 1;
        }else{
            $bab_tertinggi = $bab_tertinggi;
            $level_tertinggi = $level_tertinggi;
        }
        print_r($this->Siswa_model->get_leveling_by_id($id_leveling)->row());
        $data['dataSoal'] = $this->Soal_model->getsoal_bab_level_limit(10,7,16,$bab_tertinggi,$level_tertinggi);
        echo "<br>";
        echo "<br>";
        print_r($data['dataSoal']);
        echo "<br>";
        echo "<br>";
        echo $bab_tertinggi;
        echo "<br>";
        echo $level_tertinggi;
        echo "<br>";
        echo $bintang;
    }
    public function lalala(){
        $data['bab'] = $this->Bab_model->get_BabLevel(15);
        foreach ($data['bab'] as $key => $value) {
            $level['bab_id'] = $value->bab_id;
            $level['nama'] = $value->nama;
            $level['nama_bab'] = $value->bab;
            $level_1 = $this->Siswa_model->get_level_by_bab_mapel_user(2,15,$value->bab_id,1);
            if($level_1->num_rows() > 0){
                $level['level_1'] = $level_1->row()->bintang;
            }else{
                $level['level_1'] = 0;
            }
            $level_2 = $this->Siswa_model->get_level_by_bab_mapel_user(2,15,$value->bab_id,2);
            if($level_2->num_rows() > 0){
                $level['level_2'] = $level_2->row()->bintang;
            }else{
                $level['level_2'] = 0;
            }
            $level_3 = $this->Siswa_model->get_level_by_bab_mapel_user(2,15,$value->bab_id,3);
            if($level_3->num_rows() > 0){
                $level['level_3'] = $level_3->row()->bintang;
            }else{
                $level['level_3'] = 0;
            }
            $level_4 = $this->Siswa_model->get_level_by_bab_mapel_user(2,15,$value->bab_id,4);
            if($level_4->num_rows() > 0){
                $level['level_4'] = $level_4->row()->bintang;
            }else{
                $level['level_4'] = 0;
            }
            $bab[] = (object) $level;
        }
        foreach ($bab as $key => $value) {
            echo "<br>";
            echo $value->nama;
            echo "<br>";
            echo $value->level_1;
            echo "<br>";
            echo $value->level_2;
        }
    }
    
}
