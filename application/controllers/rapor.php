 <?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rapor extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        if ($this->session->userdata('login')) {
            $this->load->helper('url');
            $this->load->library('Login_auth');
            $this->load->model('Siswa_model');
            $this->load->model('Mapel_model');
            $this->load->model('Guru_model');
        } else {
            redirect('/welcome', 'refresh');
        }
    }

    public function index() {
        $auth = $this->login_auth->set_user_info();
        $data['siswa'] = $this->session->userdata('siswa')[0];
        // if ($auth !== NULL) {
            $this->load->view('rapor',$data);
        // } else {
        //     redirect('/welcome', 'refresh');
        // }
    }

    public function math() {
        $this->load->view('math');
    }

    public function bahasa() {
        $this->load->view('bahasa');
    }

    public function science() {
        $this->load->view('science');
    }

    public function english() {
        $this->load->view('english');
    }

    public function participants() {
        $data['soal'] = $this->input->get('t');
        $data['siswa'] = $this->session->userdata('siswa')[0];
        $data['participant'] = $this->Siswa_model->get(array("kelas_id" => $data['siswa']->id_kelas));
        switch ($data['soal']) {
            case 15:
                $data['mapel'] = 'Matematika Dasar';
                $data['icon'] = base_url('asset/images/Matematika.png');
                break;
            case 16:
                $data['mapel'] = 'Bahasa Indonesia';
                $data['icon'] = base_url('asset/images/Bahasa Indonesia.png');
                break;
            case 18:
                $data['mapel'] = 'IPA';
                $data['icon'] = base_url('asset/images/IPA.png');
                break;
            case 20:
                $data['mapel'] = 'Bahasa Inggris';
                $data['icon'] = base_url('asset/images/Bahasa Inggris.png');
                break;
            
            default:
                # code...
                break;
        }
        $this->load->view('participants',$data);
    }

    public function exam() {
        $this->load->model('Soal_model');
        $data['soal'] = $this->input->get('t');
        $data['dataSoal'] = $this->Soal_model->getsoal_limit(10,7,$this->input->get('t'));
        // echo "<pre>";
        // print_r($dataSoal);
        // echo "</pre>";
        $this->load->view('exam',$data);
    }

    public function result() {
        $this->load->view('examResult');
    }

    public function report() {
        $this->load->view('courseReport');
    }

    public function rank() {
        $this->load->view('rank');
    }

    public function rapor_guru(){
        $data['kelas_guru'] = $this->Guru_model->get_kelas_guru($this->session->userdata('guru')[0]->id);
        $data['guru'] = $this->session->userdata('guru')[0];
        $data['mapel'] = $this->Mapel_model->get_all();
        $this->load->view('guru/rapor_guru',$data);
    }
    public function nilai_rapor(){
        $data['rapor_nilai'] = $this->Mapel_model->get_rapor_bymapel();
        $data['kelas_guru'] = $this->Guru_model->get_kelas_guru($this->session->userdata('guru')[0]->id);
        $data['guru'] = $this->session->userdata('guru')[0];
        $data['mapel'] = $this->Mapel_model->get_all();
        $this->load->view('guru/rapor_kelas',$data);
    }
    
}
