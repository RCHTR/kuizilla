<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kisi_model extends CI_Model {

    private $table_name = 'm_kisikisi';
    private $table_detail = 'm_soal_detail';

    public function get_all() {
        $query = $this->db->get($this->table_name);
        return $query->result();
    }

    public function get_allJoin() {
        $this->db->select('m_kisikisi.id,logo,title,m_mapel.nama,nama_type_kisikisi,jenjang,kurikulum,username,kelas,penulis,tahun_terbit,content,username,tagar');
        $this->db->from($this->table_name);
        $this->db->join('m_mapel', 'm_mapel.id = m_kisikisi.id_mapel', 'left');
        $this->db->join('m_type_kisikisi', 'm_type_kisikisi.id = m_kisikisi.type_kisikisi_id', 'left');
        $this->db->join('m_tingkatan', 'm_tingkatan.id = m_kisikisi.tingkatan_id', 'left');
        $this->db->join('users', 'users.id = m_kisikisi.created_by', 'left');
        $this->db->order_by('m_kisikisi.id', 'desc');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_allJoinByDate($date) {
        $this->db->select('m_kisikisi.id,logo,title,m_mapel.nama,nama_type_kisikisi,jenjang,kurikulum,username,kelas,penulis,tahun_terbit,content,username,tagar');
        $this->db->from($this->table_name);
        $this->db->join('m_mapel', 'm_mapel.id = m_kisikisi.id_mapel', 'left');
        $this->db->join('m_type_kisikisi', 'm_type_kisikisi.id = m_kisikisi.type_kisikisi_id', 'left');
        $this->db->join('m_tingkatan', 'm_tingkatan.id = m_kisikisi.tingkatan_id', 'left');
        $this->db->join('users', 'users.id = m_kisikisi.created_by', 'left');
        $this->db->order_by('m_kisikisi.id', 'desc');
        $this->db->like('m_kisikisi.created_date', $date);
        $query = $this->db->get();

        return $query->result();
    }

    public function get_allBy($text) {
        //membuat variabel $where dengan nilai kosong
        $where = "";

        //membuat variabel $kata_kunci_split untuk memecah kata kunci setiap ada spasi
        $kata_kunci_split = preg_split('/[\s]+/', $text);
        //menghitung jumlah kata kunci dari split di atas
        $total_kata_kunci = count($kata_kunci_split);

        //melakukan perulangan sebanyak kata kunci yang di masukkan
        foreach ($kata_kunci_split as $key => $kunci) {
            //set variabel $where untuk query nanti
            $where .= "title LIKE '%$kunci%'";
            //jika kata kunci lebih dari 1 (2 dan seterusnya) maka di tambahkan OR di perulangannya
            if ($key != ($total_kata_kunci - 1)) {
                $where .= " OR ";
            }
        }
        //melakukan perulangan sebanyak kata kunci yang di masukkan
        foreach ($kata_kunci_split as $key => $kunci) {
            if ($key == 0) {
                $where .= " OR ";
            }
            //set variabel $where untuk query nanti
            $where .= "content LIKE '%$kunci%'";
            //jika kata kunci lebih dari 1 (2 dan seterusnya) maka di tambahkan OR di perulangannya
            if ($key != ($total_kata_kunci - 1)) {
                $where .= " OR ";
            }
        }
        foreach ($kata_kunci_split as $key => $kunci) {
            if ($key == 0) {
                $where .= " OR ";
            }
            //set variabel $where untuk query nanti
            $where .= "tagar LIKE '%$kunci%'";
            //jika kata kunci lebih dari 1 (2 dan seterusnya) maka di tambahkan OR di perulangannya
            if ($key != ($total_kata_kunci - 1)) {
                $where .= " OR ";
            }
        }
        $this->db->select('m_kisikisi.id,logo,title,m_mapel.nama,nama_type_kisikisi,jenjang,kurikulum,username,kelas,penulis,tahun_terbit,content,username,tagar');
        $this->db->from($this->table_name);
        $this->db->join('m_mapel', 'm_mapel.id = m_kisikisi.id_mapel', 'left');
        $this->db->join('m_type_kisikisi', 'm_type_kisikisi.id = m_kisikisi.type_kisikisi_id', 'left');
        $this->db->join('m_tingkatan', 'm_tingkatan.id = m_kisikisi.tingkatan_id', 'left');
        $this->db->join('users', 'users.id = m_kisikisi.created_by', 'left');
        /* $this->db->like('m_mapel.nama', $text);
          $this->db->or_like('title', $text);
          $this->db->or_like('penulis', $text);
          $this->db->or_like('jenjang', $text);
          $this->db->or_like('kurikulum', $text);
          $this->db->or_like('tahun_terbit', $text);
          $this->db->or_like('content', $text);
          $this->db->or_like('kelas', $text);
          $this->db->or_like('tagar', $text);
         */
        $this->db->where($where);
        $this->db->order_by('m_kisikisi.id', 'desc');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_by_id($id) {
        $this->db->select('m_kisikisi.id,logo,title,m_mapel.nama,nama_type_kisikisi,jenjang,kurikulum,username,kelas,penulis,tahun_terbit,content,username,tagar');
        $this->db->from($this->table_name);
        $this->db->join('m_mapel', 'm_mapel.id = m_kisikisi.id_mapel', 'left');
        $this->db->join('m_type_kisikisi', 'm_type_kisikisi.id = m_kisikisi.type_kisikisi_id', 'left');
        $this->db->join('m_tingkatan', 'm_tingkatan.id = m_kisikisi.tingkatan_id', 'left');
        $this->db->join('users', 'users.id = m_kisikisi.created_by', 'left');
        $this->db->where(array('m_kisikisi.id' => $id));
        $query = $this->db->get();
        return $query->row();
    }

    public function create($data = array()) {
        $this->db->set('created_date', 'NOW()', false);
        $this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }

    public function createDetail($data = array()) {
        $this->db->set('created_date', 'NOW()', false);
        $this->db->insert($this->table_detail, $data);
    }

    public function edit($data = array(), $id) {
        $this->db->where('id', $id);
        $this->db->set('updated_date', 'NOW()', false);

        $this->db->update($this->table_name, $data);
    }

    public function hapus($id) {

        $this->db->where('id', $id);
        $this->db->delete($this->table_name);
    }

    public function hapus_detail($id) {

        $this->db->where('soal_id', $id);
        $this->db->delete($this->table_detail);
    }

    public function getdetail_by_nourut_id($nourut, $id) {
        $query = $this->db->get_where($this->table_detail, array('soal_id' => $id, 'no_urut' => $nourut));
        return $query->row();
    }

    public function get_allJoinLimit($limit) {
        $this->db->select('m_kisikisi.id,logo,title,m_mapel.nama,nama_type_kisikisi,jenjang,kurikulum,username,kelas,penulis,tahun_terbit,content,username,tagar');
        $this->db->from($this->table_name);
        $this->db->join('m_mapel', 'm_mapel.id = m_kisikisi.id_mapel', 'left');
        $this->db->join('m_type_kisikisi', 'm_type_kisikisi.id = m_kisikisi.type_kisikisi_id', 'left');
        $this->db->join('m_tingkatan', 'm_tingkatan.id = m_kisikisi.tingkatan_id', 'left');
        $this->db->join('users', 'users.id = m_kisikisi.created_by', 'left');
        $this->db->order_by('m_kisikisi.id', 'desc');
        $this->db->limit($limit);
        $query = $this->db->get();

        return $query->result();
    }

}
