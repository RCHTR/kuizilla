<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Uji_model extends CI_Model {

    private $table_name = 'm_uji';
    private $table_detail = 'm_soal_uji';

    public function get_all() {
        $query = $this->db->get($this->table_name);
        return $query->result();
    }

    public function store($data = array()) {
        $detail = array();
        try {
            $this->db->insert($this->table_name, $data['master']);
            $master_id = $this->db->insert_id();
            foreach ($data['detail'] as $key => $value) {
                $detail[] = array(
                    'uji_id' => $master_id,
                    'soal_detil_id' => $key,
                    'nilai' => (($value) ? 10 : 0)
                );
            }
            $this->db->insert_batch($this->table_detail, $detail);
            return $master_id;
        } catch (Exception $e) {
            return 'Caught exception: '. $e->getMessage(). "\n";
        }
    }

    public function getUjiResult($id) {
        $query = $this->db->get_where($this->table_name, array('id' => $id))->result();
        $query[0]->detail = $this->db->get_where($this->table_detail, array('uji_id' => $id))->result();
        return $query;
    }

    public function getLevelSiswa($idSiswa) {
        $mapel[15] = $this->db->group_by('id_siswa')->order_by('id', 'desc')->get_where($this->table_name, array('id_siswa' => $idSiswa, 'mapel_id' => 15))->result();
        $mapel[16] = $this->db->group_by('id_siswa')->order_by('id', 'desc')->get_where($this->table_name, array('id_siswa' => $idSiswa, 'mapel_id' => 16))->result();
        $mapel[18] = $this->db->group_by('id_siswa')->order_by('id', 'desc')->get_where($this->table_name, array('id_siswa' => $idSiswa, 'mapel_id' => 18))->result();
        $mapel[20] = $this->db->group_by('id_siswa')->order_by('id', 'desc')->get_where($this->table_name, array('id_siswa' => $idSiswa, 'mapel_id' => 18))->result();

        return $mapel;

    }
}
