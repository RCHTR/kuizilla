<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mapel_model extends CI_Model {

    public function get_all() {
        $query = $this->db->get('m_mapel');
        return $query->result();
    }

    public function get_by_id($id) {
        $query = $this->db->get_where('m_mapel', array('id' => $id));
        return $query->row();
    }

    public function create($nama) {
        $data = array(
            'nama' => $nama
        );

        $this->db->insert('m_mapel', $data);
    }

    public function edit($nama, $id) {

        $this->db->set('nama', $nama);
        $this->db->where('id', $id);
        $this->db->update('m_mapel');
    }
    
     public function hapus($id) {

        $this->db->where('id', $id);
        $this->db->delete('m_mapel');
    }
    public function get_all_bab(){
        $this->db->select('*');
        $this->db->from('m_bab');
        return $this->db->get()->result();
    }
    public function get_uji_by_siswa_mapel($siswa,$mapel){
        $this->db->select('*');
        $this->db->from('m_uji');
        $this->db->where('id_siswa',$siswa);
        $this->db->where('mapel_id',$mapel);
        return $this->db->get();
    }
}
