<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Siswa_model extends CI_Model {

    private $table = 'm_siswa';

    public function get_all() {
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function login($params) {
        $this->db->select('id, nis, password')
                ->where($params);
        $check = $this->db->get($this->table)->row();

        if ($check) {
            // if(password_verify($this->input->post('password').$this->config->item('encryption_key'), $check->userPass)){
            if ($this->input->post('password') == $check->password) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function get($params) {
        $this->db->select('m_siswa.id, m_siswa.nama as nama_siswa, m_kelas.id as id_kelas, m_kelas.nama as nama_kelas, nis, m_sekolah.nama as nama_sekolah')
                ->join('m_sekolah', 'm_sekolah.id = m_siswa.sekolah_id', 'left')
                ->join('m_kelas', 'm_sekolah.id = m_siswa.kelas_id', 'left')
                ->where($params);
        return $this->db->get($this->table)->result();
    }
    public function get_current_leveling($params){
        $this->db->select('*');
        $this->db->from('m_siswa_level');
        $this->db->where('id_siswa',$params);
        return $this->db->get();
    }
    public function get_bab_tertinggi($params){
        $this->db->select_max('id_bab');
        $this->db->from('m_siswa_level');
        $this->db->where('id_siswa',$params);
        return $this->db->get();
    }
    public function get_level_tertinggi($siswa,$bab){
        $this->db->select('id, id_level,bintang');
        $this->db->from('m_siswa_level');
        $this->db->order_by('id_level','desc');
        $this->db->limit(1);
        $this->db->where('id_siswa',$siswa);
        $this->db->where('id_bab',$bab);
        return $this->db->get();
    }
    public function get_leveling_by_id($params){
        $this->db->select('*');
        $this->db->from('m_siswa_level');
        $this->db->where('id',$params);
        return $this->db->get();
    }
    public function insert_leveling($data){
        $this->db->insert('m_siswa_level', $data);
    }
    public function get_uji_by_siswa_mapel($siswa,$mapel){
        $this->db->select('*');
        $this->db->from('m_uji');
        $this->db->where('id_siswa',$siswa);
        $this->db->where('mapel_id',$mapel);
        return $this->db->get();
    }
    public function get_bab_level_mapel($mapel){

    }
    public function get_level_by_bab_mapel_user($siswa,$mapel,$bab,$level){
        $this->db->select('*');
        $this->db->from('m_siswa_level');
        $this->db->where('id_siswa',$siswa);
        $this->db->where('mapel_id',$mapel);
        $this->db->where('id_bab',$bab);
        $this->db->where('id_level',$level);
        $this->db->order_by('bintang','desc');
        $this->db->limit(1);
        return $this->db->get();
    }
}
