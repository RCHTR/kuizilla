<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Guru_model extends CI_Model {

    private $table = 'm_pegawai_sekolah';

    public function get_all() {
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function login($params) {
        $this->db->select('id, nip, telepon')
                ->where($params);
        $check = $this->db->get($this->table)->row();

        if ($check) {
            if ($this->input->post('password') == $check->telepon) {
                return true;
            } else {
                return false;
            }
        }
    }
    public function get($params) {
        $this->db->select('m_pegawai_sekolah.id, m_pegawai_sekolah.nama as nama_guru, m_kelas.id as id_kelas, m_kelas.nama as nama_kelas, nip, m_sekolah.nama as nama_sekolah, m_sekolah.propinsi as propinsi_sekolah')
                ->join('m_sekolah', 'm_sekolah.id = m_pegawai_sekolah.sekolah_id', 'left')
                ->join('m_kelas', 'm_kelas.guru_id = m_pegawai_sekolah.id', 'left')
                ->where($params);
        return $this->db->get($this->table)->result();
    }
    public function get_kelas_guru($params){
        $this->db->select('*');
        $this->db->from('m_kelas');
        $this->db->where(array('guru_id' => $params));
        return $this->db->get()->result();
    }
    public function get_kelas_guru_byidkelas($params){
        $this->db->select('*');
        $this->db->from('m_kelas');
        $this->db->where(array('id' => $params));
        return $this->db->get()->row();
    }

}
