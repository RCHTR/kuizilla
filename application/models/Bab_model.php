<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bab_model extends CI_Model {

    private $table_name = 'm_bab';

    public function get_all() {
        $query = $this->db->get($this->table_name);
        return $query->result();
    }

    public function get_BabLevel($mapelId){
        $this->db->from($this->table_name);
        $this->db->where('m_soal.mapel_id', $mapelId);
        $this->db->join('m_soal', 'm_soal.bab_id = m_bab.id');
        // $this->db->group_by('m_soal.bab_id');
        $this->db->order_by('m_bab.id', 'ASC');
        $query = $this->db->get();

        return $query->result();
    }
}
