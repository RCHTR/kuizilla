<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Soal_model extends CI_Model {

    private $table_name = 'm_soal';
    private $table_detail = 'm_soal_detail';

    public function get_all() {
        $query = $this->db->get($this->table_name);
        return $query->result();
    }

    public function get_allJoin() {
        $this->db->select('m_soal.id,logo,subjek,m_mapel.nama,nama_type_soal,jenjang,kelas,kurikulum,username,nama_meta_sumber,skor_tertinggi');
        $this->db->from($this->table_name);
        $this->db->join('m_mapel', 'm_mapel.id = m_soal.mapel_id', 'left');
        $this->db->join('m_type_soal', 'm_type_soal.id = m_soal.type_soal_id', 'left');
        $this->db->join('m_tingkatan', 'm_tingkatan.id = m_soal.tingkatan_id', 'left');
        $this->db->join('m_meta_sumber', 'm_meta_sumber.id = m_soal.meta_sumber_id', 'left');
        $this->db->join('users', 'users.id = m_soal.created_by', 'left');
        $this->db->order_by('m_soal.id', 'desc');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_allByMapelID($mapelId) {
        $this->db->select('m_soal.id,logo,subjek,m_mapel.nama,nama_type_soal,jenjang,kelas,kurikulum,username,nama_meta_sumber,skor_tertinggi');
        $this->db->from($this->table_name);
        $this->db->join('m_mapel', 'm_mapel.id = m_soal.mapel_id', 'left');
        $this->db->join('m_type_soal', 'm_type_soal.id = m_soal.type_soal_id', 'left');
        $this->db->join('m_tingkatan', 'm_tingkatan.id = m_soal.tingkatan_id', 'left');
        $this->db->join('m_meta_sumber', 'm_meta_sumber.id = m_soal.meta_sumber_id', 'left');
        $this->db->join('users', 'users.id = m_soal.created_by', 'left');
        $this->db->where('m_mapel.id', $mapelId);
        $this->db->order_by('m_soal.id', 'desc');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_allBy($text) {
        //membuat variabel $where dengan nilai kosong
        $where = "";

        //membuat variabel $kata_kunci_split untuk memecah kata kunci setiap ada spasi
        $kata_kunci_split = preg_split('/[\s]+/', $text);
        //menghitung jumlah kata kunci dari split di atas
        $total_kata_kunci = count($kata_kunci_split);

        //melakukan perulangan sebanyak kata kunci yang di masukkan
        foreach ($kata_kunci_split as $key => $kunci) {
            //set variabel $where untuk query nanti
            $where .= "subjek LIKE '%$kunci%'";
            //jika kata kunci lebih dari 1 (2 dan seterusnya) maka di tambahkan OR di perulangannya
            if ($key != ($total_kata_kunci - 1)) {
                $where .= " OR ";
            }
        }
        //melakukan perulangan sebanyak kata kunci yang di masukkan
        foreach ($kata_kunci_split as $key => $kunci) {
            if ($key == 0) {
                $where .= " OR ";
            }
            //set variabel $where untuk query nanti
            $where .= "jenjang LIKE '%$kunci%'";
            //jika kata kunci lebih dari 1 (2 dan seterusnya) maka di tambahkan OR di perulangannya
            if ($key != ($total_kata_kunci - 1)) {
                $where .= " OR ";
            }
        }
        foreach ($kata_kunci_split as $key => $kunci) {
            if ($key == 0) {
                $where .= " OR ";
            }
            //set variabel $where untuk query nanti
            $where .= "tagar LIKE '%$kunci%'";
            //jika kata kunci lebih dari 1 (2 dan seterusnya) maka di tambahkan OR di perulangannya
            if ($key != ($total_kata_kunci - 1)) {
                $where .= " OR ";
            }
        }
        $this->db->select('m_soal.id,logo,subjek,m_mapel.nama,nama_type_soal,jenjang,kelas,kurikulum,username,nama_meta_sumber,skor_tertinggi');
        $this->db->from($this->table_name);
        $this->db->join('m_mapel', 'm_mapel.id = m_soal.mapel_id', 'left');
        $this->db->join('m_type_soal', 'm_type_soal.id = m_soal.type_soal_id', 'left');
        $this->db->join('m_tingkatan', 'm_tingkatan.id = m_soal.tingkatan_id', 'left');
        $this->db->join('m_meta_sumber', 'm_meta_sumber.id = m_soal.meta_sumber_id', 'left');
        $this->db->join('users', 'users.id = m_soal.created_by', 'left');
        /*
          $this->db->like('m_mapel.nama',$text);
          $this->db->or_like('subjek',$text);
          $this->db->or_like('nama_type_soal',$text);
          $this->db->or_like('jenjang',$text);
          $this->db->or_like('nama_meta_sumber',$text);
          $this->db->or_like('kelas',$text);
         */
        $this->db->where($where);
        $this->db->order_by('m_soal.id', 'desc');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_allOrder($order) {
        $this->db->select('m_soal.id,logo,subjek,m_mapel.nama,nama_type_soal,jenjang,kelas,kurikulum,username,nama_meta_sumber,tahun_terbit,skor_tertinggi');
        $this->db->from($this->table_name);
        $this->db->join('m_mapel', 'm_mapel.id = m_soal.mapel_id', 'left');
        $this->db->join('m_type_soal', 'm_type_soal.id = m_soal.type_soal_id', 'left');
        $this->db->join('m_tingkatan', 'm_tingkatan.id = m_soal.tingkatan_id', 'left');
        $this->db->join('m_meta_sumber', 'm_meta_sumber.id = m_soal.meta_sumber_id', 'left');
        $this->db->join('users', 'users.id = m_soal.created_by', 'left');
        $this->db->order_by($order, 'asc');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_allJoinById($id) {
        $this->db->select('m_soal.id,logo,subjek,m_mapel.nama,nama_type_soal,jenjang,kelas,kurikulum,username,nama_meta_sumber,IFNULL(skor_tertinggi,0) as skor_tertinggi');
        $this->db->from($this->table_name);
        $this->db->join('m_mapel', 'm_mapel.id = m_soal.mapel_id', 'left');
        $this->db->join('m_type_soal', 'm_type_soal.id = m_soal.type_soal_id', 'left');
        $this->db->join('m_tingkatan', 'm_tingkatan.id = m_soal.tingkatan_id', 'left');
        $this->db->join('m_meta_sumber', 'm_meta_sumber.id = m_soal.meta_sumber_id', 'left');
        $this->db->join('users', 'users.id = m_soal.created_by', 'left');
        $this->db->where('m_soal.id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    public function getdetail_by_nourut_id($nourut, $id) {
        $query = $this->db->get_where($this->table_detail, array('soal_id' => $id, 'no_urut' => $nourut));
        return $query->row();
    }

    public function getsoal_limit($limit, $id, $mapel) {
        $query = $this->db->select('m_soal_detail.id,pertanyaan,pilihan_a,pilihan_b,pilihan_c,pilihan_d')->limit($limit)
                    ->join('m_soal', 'm_soal.id = m_soal_detail.soal_id', 'left')
                    ->join('m_type_soal', 'm_type_soal.id = m_soal.type_soal_id', 'left')
                    ->order_by('no_urut','ASC')
                    ->get_where($this->table_detail, array('type_soal_id' => $id, 'mapel_id' => $mapel));
        return $query->result();
    }
    public function getsoal_bab_level_limit($limit, $id, $mapel, $bab, $level) {
        $query = $this->db->select('m_soal_detail.id,pertanyaan,pilihan_a,pilihan_b,pilihan_c,pilihan_d')->limit($limit)
                    ->join('m_soal', 'm_soal.id = m_soal_detail.soal_id', 'left')
                    ->join('m_type_soal', 'm_type_soal.id = m_soal.type_soal_id', 'left')
                    ->order_by('no_urut','ASC')
                    ->get_where($this->table_detail, array('type_soal_id' => $id, 'mapel_id' => $mapel, 'level_id' => $level, 'bab_id' => $bab));
        return $query->result();
    }

    public function getdetail_by_id($id) {
        $query = $this->db->get_where($this->table_detail, array('soal_id' => $id));
        return $query->result();
    }

    public function get_by_id($id) {
        $query = $this->db->get_where($this->table_name, array('id' => $id));
        return $query->row();
    }

    public function updateBintang($data = array(), $id) {
        $this->db->where('id', $id);
        $this->db->update($this->table_name, $data);
    }

    public function getJawaban($id,$jawaban) {
        return $this->db->get_where($this->table_detail, array('id' => $id, 'jawaban' => $jawaban));
    }    

}
