<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login_auth {

    function __construct() {
        $this->ci = & get_instance();
        $this->ci->load->library('session');
    }

    function set_user_info() {
        $this->ci->load->model('login_auth/login');
        $ip = $this->ci->input->ip_address();
        $data = $this->ci->login->get_by_ip($ip);
        $this->ci->session->set_userdata(array(
            'user' => $data,
        ));
    }

    function get_user_info() {
        return $this->ci->session->userdata('user');
    }

}
